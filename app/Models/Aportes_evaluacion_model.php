<?php

namespace App\Models;

use CodeIgniter\Model;

class Aportes_evaluacion_model extends Model
{
    protected $table      = 'sw_aporte_evaluacion';
    // Uncomment below if you want add primary key
    protected $primaryKey = 'id_aporte_evaluacion';
    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['id_periodo_evaluacion', 'id_tipo_aporte', 'ap_nombre', 'ap_abreviatura', 'ap_shortname', 'ap_fecha_apertura', 'ap_fecha_cierre'];

    public function listarAportesPorPeriodoId($id_periodo)
    {
        $aportes = $this->db->query("
            SELECT a.*
              FROM sw_aporte_evaluacion a,
                   sw_periodo_evaluacion p 
             WHERE p.id_periodo_evaluacion = a.id_periodo_evaluacion
               AND a.id_periodo_evaluacion = $id_periodo 
             ORDER BY id_aporte_evaluacion              
        ");

        return $aportes->getResultObject();
    }
}
