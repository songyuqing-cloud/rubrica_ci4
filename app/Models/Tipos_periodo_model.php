<?php

namespace App\Models;

use CodeIgniter\Model;

class Tipos_periodo_model extends Model
{

    protected $table      = 'sw_tipo_periodo';
    protected $primaryKey = 'id_tipo_periodo';

    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['tp_descripcion'];

}
