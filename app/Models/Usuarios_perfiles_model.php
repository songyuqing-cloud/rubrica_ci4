<?php

namespace App\Models;

use CodeIgniter\Model;

class Usuarios_perfiles_model extends Model
{

    protected $table      = 'sw_usuario_perfil';

    protected $returnType     = 'object';

    protected $allowedFields = ['id_usuario', 'id_perfil'];

}
