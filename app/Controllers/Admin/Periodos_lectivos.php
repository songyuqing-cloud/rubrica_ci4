<?php 
namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use CodeIgniter\Exceptions\PageNotFoundException;

class Periodos_lectivos extends BaseController
{

    public function index()
    {
        $model = model('Periodos_lectivos_model');
        
        return view('Admin/Periodos_lectivos/index', [
            'periodos_lectivos' => $model
                                    ->join(
                                        'sw_periodo_estado', 
                                        'sw_periodo_estado.id_periodo_estado = sw_periodo_lectivo.id_periodo_estado'
                                    )->paginate(config('Blog')->regPerPage),
            'pager'             => $model->pager
        ]);
    }

    public function create()
    {
        $model = model('Modalidades_model');
        return view('Admin/Periodos_lectivos/create', [
            'modalidades' => $model->findAll()
        ]);
    }

    public function store()
    {
        if (!$this->validate([
            'mo_nombre' => 'required|max_length[64]|is_unique[sw_modalidad.mo_nombre]'
        ])) 
        {
            return redirect()->back()->withInput()
                ->with('msg', [
                    'type' => 'danger',
                    'icon' => 'ban',
                    'body' => 'Tienes campos incorrectos.'
                ])
                ->with('errors', $this->validator->getErrors());
        }

        $model = model('Modalidades_model');

        $model->save([
            'mo_nombre' => trim($_POST['mo_nombre']),
            'mo_activo' => $_POST['mo_activo']
        ]);

        return redirect()->route('modalidades')->with('msg', [
            'type' => 'success',
            'icon' => 'check',
            'body' => 'La modalidad fue guardada correctamente.'
        ]);
    }

    public function edit(string $id)
    {
        $model = model('Modalidades_model');
        
        if (!$modalidad = $model->find($id)) {
            throw PageNotFoundException::forPageNotFound();
        }

        return view('Admin/Modalidades/edit', [
            'modalidad' => $modalidad
        ]);
    }

    public function update()
    {
        if (!$this->validate([
            'mo_nombre'    => 'required|max_length[64]',
            'id_modalidad' => 'required|is_not_unique[sw_modalidad.id_modalidad]'
        ])) 
        {
            return redirect()->back()->withInput()
                ->with('msg', [
                    'type' => 'danger',
                    'icon' => 'ban',
                    'body' => 'Tienes campos incorrectos.'
                ])
                ->with('errors', $this->validator->getErrors());
        }

        $model = model('Modalidades_model');

        $model->save([
            'id_modalidad' => $_POST['id_modalidad'],
            'mo_nombre' => trim($_POST['mo_nombre']),
            'mo_activo' => $_POST['mo_activo']
        ]);

        return redirect('modalidades')->with('msg', [
            'type' => 'success',
            'icon' => 'check',
            'body' => 'La modalidad fue actualizada correctamente.'
        ]);
    }

    public function delete(string $id)
    {
        try {
            $model = model('Modalidades_model');
            $model->delete($id);
    
            return redirect('modalidades')->with('msg', [
                'type' => 'success',
                'icon' => 'check',
                'body' => 'La modalidad fue eliminada correctamente.'
            ]);
        } catch (\Exception $e) {
            return redirect('modalidades')->with('msg', [
                'type' => 'danger',
                'icon' => 'ban',
                'body' => 'La modalidad no se pudo eliminar exitosamente...Error: ' . $e->getMessage()
            ]);
        }
    }
}