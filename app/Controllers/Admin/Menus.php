<?php 
namespace App\Controllers\Admin;

use App\Models\Menus_model;
use App\Models\Perfiles_model;
use App\Controllers\BaseController;

class Menus extends BaseController
{
    protected $menus,
              $perfiles;
    protected $reglas;

    public function __construct()
    {
        $this->menus = new Menus_model();
        $this->perfiles = new Perfiles_model();

        $this->reglas = [
            'id_perfil' => [
                'rules' => 'required|is_not_unique[sw_perfil.id_perfil]',
                'errors' => [
                    'required' => 'El campo Perfil es obligatorio.',
                    'is_not_unique' => 'No existe la opción elegida en la base de datos.'
                ]
            ],
            'mnu_texto' => [
                'rules' => 'required|max_length[32]',
                'errors' => [
                    'required' => 'El campo Texto es obligatorio.',
                    'max_length' => 'El campo Texto no debe exceder los 32 caracteres.'
                ]
            ],
            'mnu_link' => [
                'rules' => 'required|max_length[64]',
                'errors' => [
                    'required' => 'El campo Enlace es obligatorio.',
                    'max_length' => 'El campo Texto no debe exceder los 64 caracteres.'
                ]
            ],
            'mnu_publicado' => [
                'rules' => 'required|is_not_unique[sw_menu.mnu_publicado]',
                'errors' => [
                    'required' => 'El campo Publicado es obligatorio.',
                    'is_not_unique' => 'No existe la opción elegida en la base de datos.'
                ]
            ]
        ];
    }

    public function index()
    {
        $perfiles = $this->perfiles->listarPerfiles();

        return view('Admin/Menus/index', ['perfiles' => $perfiles]);
    }

    public function create()
    {
        $perfiles = $this->perfiles->listarPerfiles();

        return view('Admin/Menus/create', ['perfiles' => $perfiles]);
    }

    public function store()
    {
        $data = [
            'mnu_texto'      => trim($_POST['mnu_texto']),
            'mnu_link'       => $_POST['mnu_link'],
            'mnu_padre'      => $_POST['mnu_padre'],
            'mnu_publicado'  => $_POST['mnu_publicado'],
            'mnu_nivel'      => $_POST['mnu_nivel']
        ];

        $id_perfil = $_POST['id_perfil'];

        if($this->menus->crearMenu($data, $id_perfil)) {
            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "El menú fue insertado exitosamente.",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);
        } else {
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "El menú no se pudo insertar...",
				"tipo_mensaje" => "error"
			);
			echo json_encode($data);
        }
    }

    public function update()
    {
        $data = [
            'id_menu'        => $_POST['id_menu'],
            'mnu_texto'      => trim($_POST['mnu_texto']),
            'mnu_link'       => $_POST['mnu_link'],
            'mnu_padre'      => $_POST['mnu_padre'],
            'mnu_publicado'  => $_POST['mnu_publicado'],
            'mnu_nivel'      => $_POST['mnu_nivel']
        ];

        if($this->menus->save($data)){
            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "El menú fue actualizado exitosamente.",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);
        }else{
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "El menú no se pudo actualizar...",
				"tipo_mensaje" => "error"
			);
			echo json_encode($data);
        }

    }

    public function getMenusByRoleId()
    {
        $id_perfil = $_POST['id_perfil'];
        echo json_encode($this->menus->listarMenusNivel1($id_perfil));
    }

    public function getMenusByParentId()
    {
        $id_menu = $_POST['id_menu'];
        echo json_encode($this->menus->listarMenusHijos($id_menu));
    }

    public function getMenusById()
    {
        $id_menu = $_POST['id_menu'];
        echo json_encode($this->menus->getMenusById($id_menu));
    }

    public function saveNewPositions()
    {
        foreach($_POST['positions'] as $position) {
            $index = $position[0];
            $newPosition = $position[1];

            $this->menus->actualizarOrden($index, $newPosition);
        }
    }
}