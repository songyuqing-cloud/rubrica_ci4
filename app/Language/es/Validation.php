<?php

// override core en language system validation or define your own en language validation message
return [
    // Core Messages
    'required'       => "El campo {field} es obligatorio.",
    'isset'          => "El campo {field} debe contener un valor.",
    'valid_email'    => "El campo {field} debe contener una dirección de correo válida.",
    'valid_emails'   => "El campo {field} debe contener todas las direcciones de correo válidas.",
    'valid_url'      => "El campo {field} debe contener una URL válida.",
    'valid_ip'       => "El campo {field} debe contener una dirección IP válida.",
    'min_length'     => "El campo {field} debe contener al menos %s caracteres de longitud.",
    'max_length'     => "El campo {field} no debe exceder los %s caracteres de longitud.",
    'exact_length'   => "El campo {field} debe tener exactamente %s carácteres.",
    'alpha'          => "El campo {field} sólo puede contener carácteres alfabéticos.",
    'alpha_numeric'  => "El campo {field} sólo puede contener carácteres alfanuméricos.",
    'alpha_dash'     => "El campo {field} sólo puede contener carácteres alfanuméricos, guiones bajos '_' o guiones '-'.",
    'numeric'        => "El campo {field}s sólo puede contener números.",
    'is_numeric'     => "El campo {field} sólo puede contener carácteres numéricos.",
    'integer'        => "El campo {field} debe contener un número entero.",
    'regex_match'    => "El campo {field} no tiene el formato correcto.",
    'matches'        => "El campo {field} no concuerda con el campo {field} .",
    'is_natural'     => "El campo {field} debe contener sólo números positivos.",
    'is_natural_no_zero' => "El campo {field} debe contener un número mayor que 0.",
    'decimal'        => "El campo {field} debe contener un número decimal.",
    'less_than'      => "El campo {field} debe contener un número menor que {field}.",
    'greater_than'   => "El campo {field} debe contener un número mayor que {field}.",
    /* Added after 2.0.2 */
    'is_unique'      => "El campo {field} debe contener un valor único.",
];
