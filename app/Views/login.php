<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIAE Web 2 | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/public/bootstrap/css/bootstrap.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/public/font-awesome/css/font-awesome.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/public/dist/css/AdminLTE.min.css">

  <!-- Estilos propios de esta pagina -->
  <style type="text/css">
    .error {
      color: #F00;
      display: none;
    }

    .blanco {
      color: #FFF;
    }
  </style>

</head>

<body class="hold-transition login-page" style="background: url('<?php echo base_url(); ?>/public/images/loginFont.jpg')">
  <div class="login-box blanco">
    <div class="login-logo">
      <h2>S. I. A. E.</h2>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg">Introduzca sus datos de ingreso</p>
      <?php if (session('msg')) : ?>
        <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
        </div>
      <?php endif ?>
      <form id="form-login" action="<?php echo base_url(route_to('signin')); ?>" method="post" autocomplete="off">
        <div class="form-group has-feedback">
          <input type="text" class="form-control" placeholder="Usuario" id="username" name="username" autofocus>
          <span class="form-control-feedback">
            <img src="<?php echo base_url(); ?>/public/images/if_user_male_172625.png" height="16px" width="16px">
          </span>
          <span class="help-desk error" id="mensaje1">Debe ingresar su nombre de Usuario</span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control" placeholder="Password" id="password" name="password" autocomplete="on">
          <span class="form-control-feedback">
            <img src="<?php echo base_url(); ?>/public/images/if_91_171450.png" height="16px" width="16px">
          </span>
          <span class="help-desk error" id="mensaje2">Debe ingresar su Password</span>
        </div>
        <div class="form-group has-feedback">
          <select class="form-control" id="cboPeriodo" name="cboPeriodo">
            <option value="">Seleccione el periodo lectivo...</option>
            <?php foreach ($periodos_lectivos as $periodo_lectivo) : ?>
              <option value="<?php echo $periodo_lectivo->id_periodo_lectivo; ?>">
                <?php echo $periodo_lectivo->pe_anio_inicio . " - " . $periodo_lectivo->pe_anio_fin . " [" . $periodo_lectivo->pe_descripcion . "]"; ?>
              </option>
            <?php endforeach ?>
          </select>
          <span class="help-desk error" id="mensaje3">Debe seleccionar el periodo lectivo</span>
        </div>
        <div class="form-group has-feedback">
          <select class="form-control" id="cboPerfil" name="cboPerfil">
            <option value="">Seleccione su perfil...</option>
            <?php foreach ($perfiles as $perfil) : ?>
              <option value="<?php echo $perfil->id_perfil; ?>"><?php echo $perfil->pe_nombre; ?></option>
            <?php endforeach ?>
          </select>
          <span class="help-desk error" id="mensaje4">Debe seleccionar su perfil</span>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <button type="submit" class="btn btn-raised btn-danger btn-block" id="btnEnviar">Ingresar</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.login-box-body -->
  </div>
  <!-- /.login-box -->

  <footer style="text-align: center; color: white; margin-top: -40px;">
    .: &copy; <?php echo date("  Y"); ?> - <?php echo $nombreInstitucion; ?> :.
  </footer>

  <script src="<?php echo base_url(); ?>/public/js/jquery-3.5.1.min.js"></script>
  <script src="<?php echo base_url(); ?>/public/js/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>/public/bootstrap/js/bootstrap.js"></script>

  <script>
    $(document).ready(function() {
      $("#btnEnviar").click(function() {
        nombre = $("#username").val();
        password = $("#password").val();
        periodo = $("#cboPeriodo").val();
        perfil = $("#cboPerfil").val();
        errors = 0;

        if (nombre == "") {
          $("#mensaje1").fadeIn("slow");
          errors++;
        } else {
          $("#mensaje1").fadeOut();
        }
        if (password == "") {
          $("#mensaje2").fadeIn("slow");
          errors++;
        } else {
          $("#mensaje2").fadeOut();
        }
        if (periodo == "") {
          $("#mensaje3").fadeIn("slow");
          errors++;
        } else {
          $("#mensaje3").fadeOut();
        }
        if (perfil == "") {
          $("#mensaje4").fadeIn("slow");
          errors++;
        } else {
          $("#mensaje4").fadeOut();
        }

        if (errors > 0) {
          return false;
        }
      });
    });
  </script>
</body>

</html>