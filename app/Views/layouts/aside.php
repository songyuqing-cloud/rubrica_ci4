        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo base_url()?>/public/uploads/<?php echo session("foto") ?>" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?php echo session('rol_name') ?></p>
                    <small><?php echo session('periodo') ?></small>
                </div>
            </div>
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MENU</li>
                    <li class="<?= service('request')->uri->getPath() == 'auth/dashboard' ? 'active' : '' ?>">
                        <a href="<?php echo base_url();?>/auth/dashboard">
                            <i class="fa fa-home"></i> <span>Dashboard</span>
                        </a>
                    </li>
                    <?php 
                        $model = model('Menus_model');
                        $menusNivel1 = $model->listarMenusNivel1(session("id_perfil")); 
                        foreach($menusNivel1 as $menu) {
                    ?>
                        <?php if(count($model->listarMenusHijos($menu->id_menu)) > 0) { ?>
                            <li class="treeview">                        
                                <a href="#">
                                    <i class="fa fa-laptop"></i> <span><?php echo $menu->mnu_texto; ?></span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <?php foreach($model->listarMenusHijos($menu->id_menu) as $menu2) { ?>
                                        <li class="<?= service('request')->uri->getPath() == $menu2->mnu_link ? 'active' : '' ?>">
                                            <a href="<?php echo base_url() . '/' . $menu2->mnu_link; ?>"><i class="fa fa-circle-o"></i> <?php echo $menu2->mnu_texto; ?></a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li> 
                        <?php } else { ?>
                            <li class="<?= service('request')->uri->getPath() == $menu2->mnu_link ? 'active' : '' ?>">                        
                                <a href="<?php echo base_url() . '/' . $menu->mnu_link; ?>">
                                    <i class="fa fa-laptop"></i> <span><?php echo $menu->mnu_texto; ?></span>
                                </a>
                            </li> -->
                        <?php } ?>
                    <?php } ?>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- =============================================== -->
