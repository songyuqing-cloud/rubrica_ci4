<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Aportes de Evaluación
<?= $this->endsection('titulo') ?>

<?= $this->section('styles') ?>
    <!-- jquery-ui -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/public/jquery-ui/jquery-ui.css">
<?= $this->endsection('styles') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Aportes de Evaluación
        <small>Editar</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-success">
                        <div id="titulo" class="panel-heading">Editar Aporte de Evaluación: <?= $aporte_evaluacion->ap_nombre ?> </div>
                    </div>
                    <div class="panel-body">
                        <?php if (session('msg')) : ?>
                            <?php if (session('msg')) : ?>
                                <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                        <form id="frm-aporte-evaluacion" action="<?= base_url((route_to('aportes_evaluacion_update'))) ?>" method="post">
                            <input type="hidden" name="id_aporte_evaluacion" value="<?= $aporte_evaluacion->id_aporte_evaluacion ?>">
                            <div class="form-group <?= session('errors.ap_nombre') ? 'has-error' : '' ?>">
                                <label for="ap_nombre">Nombre:</label>
                                <input type="text" name="ap_nombre" id="ap_nombre" value="<?= old('ap_nombre') ?? $aporte_evaluacion->ap_nombre ?>" class="form-control" autofocus>
                                <span class="help-block"><?= session('errors.ap_nombre') ?></span>
                            </div>
                            <div class="form-group <?= session('errors.ap_abreviatura') ? 'has-error' : '' ?>">
                                <label for="ap_abreviatura">Abreviatura:</label>
                                <input type="text" name="ap_abreviatura" id="ap_abreviatura" value="<?= old('ap_abreviatura') ?? $aporte_evaluacion->ap_abreviatura ?>" class="form-control">
                                <span class="help-block"><?= session('errors.ap_abreviatura') ?></span>
                            </div>
                            <div class="form-group <?= session('errors.ap_fecha_apertura') ? 'has-error' : '' ?>">
                                <label for="ap_fecha_apertura">Fecha de inicio:</label>
                                <div class="controls">
                                    <div class="input-group date">
                                        <input type="text" name="ap_fecha_apertura" id="ap_fecha_apertura" class="form-control" value="<?= old('ap_fecha_apertura') ?? $aporte_evaluacion->ap_fecha_apertura ?>">
                                        <label class="input-group-addon generic-btn" style="cursor: pointer;" onclick="$('#ap_fecha_apertura').focus();"><i class="fa fa-calendar" aria-hidden="true"></i></label>
                                    </div>
                                </div>
                                <span class="help-block"><?= session('errors.ap_fecha_apertura') ?></span>
                            </div>
                            <div class="form-group <?= session('errors.ap_fecha_cierre') ? 'has-error' : '' ?>">
                                <label for="ap_fecha_cierre">Fecha de fin:</label>
                                <div class="controls">
                                    <div class="input-group date">
                                        <input type="text" name="ap_fecha_cierre" id="ap_fecha_cierre" class="form-control"  value="<?= old('ap_fecha_cierre') ?? $aporte_evaluacion->ap_fecha_cierre ?>">
                                        <label class="input-group-addon generic-btn" style="cursor: pointer;" onclick="$('#ap_fecha_cierre').focus();"><i class="fa fa-calendar" aria-hidden="true"></i></label>
                                    </div>
                                </div>
                                <span class="help-block"><?= session('errors.ap_fecha_cierre') ?></span>
                            </div>
                            <div class="form-group <?= session('errors.id_periodo_evaluacion') ? 'has-error' : '' ?>">
                                <label for="id_periodo_evaluacion">Periodo:</label>
                                <select name="id_periodo_evaluacion" id="id_periodo_evaluacion" class="form-control">
                                    <?php foreach($periodos_evaluacion as $v): ?>
                                        <option value="<?= $v->id_periodo_evaluacion ?>" <?= old('id_periodo_evaluacion') == $v->id_periodo_evaluacion ? 'selected' : ($v->id_periodo_evaluacion == $aporte_evaluacion->id_periodo_evaluacion ? 'selected' : '') ?>>
                                            <?= $v->pe_nombre ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="help-block"><?= session('errors.id_periodo_evaluacion') ?></span>
                            </div>
                            <div class="form-group <?= session('errors.id_tipo_aporte') ? 'has-error' : '' ?>">
                                <label for="id_tipo_aporte">Tipo:</label>
                                <select name="id_tipo_aporte" id="id_tipo_aporte" class="form-control">
                                    <?php foreach($tipos_aporte as $v): ?>
                                        <option value="<?= $v->id_tipo_aporte ?>" <?= old('id_tipo_aporte') == $v->id_tipo_aporte ? 'selected' : ($v->id_tipo_aporte == $aporte_evaluacion->id_tipo_aporte ? 'selected' : '') ?>>
                                            <?= $v->ta_descripcion ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="help-block"><?= session('errors.id_tipo_aporte') ?></span>
                            </div>
                            <div class="form-group">
                                <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                <a href="<?= base_url(route_to('aportes_evaluacion')) ?>" class="btn btn-default">Regresar</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>

<?= $this->section('scriptsPlugins') ?>
    <!-- jquery-ui -->
    <script src="<?php echo base_url(); ?>/public/jquery-ui/jquery-ui.js"></script>
<?= $this->endsection('scriptsPlugins') ?>

<?= $this->section('scripts') ?>
<script>
    $(document).ready(function(){
        // JQuery Listo para utilizar
        $("#ap_fecha_apertura").datepicker({
            dateFormat : 'yy-mm-dd',
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
            firstDay: 1
        });
        $("#ap_fecha_cierre").datepicker({
            dateFormat : 'yy-mm-dd',
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
            firstDay: 1
        });
    });
</script>
<?= $this->endsection('scripts') ?>