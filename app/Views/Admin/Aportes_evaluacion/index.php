<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Aportes de Evaluación
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Aportes de Evaluación
        <small>Listado</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12 table-responsive">
                    <?php if (session('msg')) : ?>
                        <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                        </div>
                    <?php endif ?>
                    <a class="btn btn-primary" href="<?= base_url(route_to('aportes_evaluacion_create')) ?>">Crear Aporte de Evaluación</a>
                    <hr>
                    <table id="t_aportes_evaluacion" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Periodo</th>
                                <th>Nombre</th>
                                <th>Tipo</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_aportes_evaluacion">
                            <?php foreach ($aportes_evaluacion as $v) : ?>
                                <tr>
                                    <td><?= $v->id_aporte_evaluacion ?></td>
                                    <td><?= $v->pe_nombre ?></td>
                                    <td><?= $v->ap_nombre ?></td>
                                    <td><?= $v->ta_descripcion ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="<?= base_url(route_to('aportes_evaluacion_edit', $v->id_aporte_evaluacion)) ?>" class="btn btn-warning btn-sm" title="Editar"><span class="fa fa-pencil"></span></a>
                                            <a href="<?= base_url(route_to('aportes_evaluacion_delete', $v->id_aporte_evaluacion)) ?>" class="btn btn-danger btn-sm" title="Eliminar"><span class="fa fa-remove"></span></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                    <?= $pager->links() ?>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>