<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Insumos de Evaluación
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Insumos de Evaluación
        <small>Listado</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12 table-responsive">
                    <a class="btn btn-primary" href="<?= base_url(route_to('rubricas_evaluacion_create')) ?>">Crear Insumo de Evaluación</a>
                    <?php if (session('msg')) : ?>
                        <div class="alert alert-<?= session('msg.type') ?> alert-dismissible" style="margin-top: 2px">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                        </div>
                    <?php endif ?>
                    <hr>
                    <table id="t_rubricas_evaluacion" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Periodo</th>
                                <th>Aporte</th>
                                <th>Nombre</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_rubricas_evaluacion">
                            <?php foreach ($rubricas_evaluacion as $v) : ?>
                                <tr>
                                    <td><?= $v->id_rubrica_evaluacion ?></td>
                                    <td><?= $v->pe_nombre ?></td>
                                    <td><?= $v->ap_nombre ?></td>
                                    <td><?= $v->ru_nombre ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="<?= base_url(route_to('rubricas_evaluacion_edit', $v->id_rubrica_evaluacion)) ?>" class="btn btn-warning btn-sm" title="Editar"><span class="fa fa-pencil"></span></a>
                                            <a href="<?= base_url(route_to('rubricas_evaluacion_delete', $v->id_rubrica_evaluacion)) ?>" class="btn btn-danger btn-sm" title="Eliminar"><span class="fa fa-remove"></span></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                    <?= $pager->links() ?>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>