<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Insumos de Evaluación
<?= $this->endsection('titulo') ?>

<?= $this->section('styles') ?>
<!-- jquery-ui -->
<link rel="stylesheet" href="<?php echo base_url(); ?>/public/jquery-ui/jquery-ui.css">
<?= $this->endsection('styles') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Insumos de Evaluación
        <small>Crear</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-success">
                        <div id="titulo" class="panel-heading">Nuevo Insumo de Evaluación</div>
                    </div>
                    <div class="panel-body">
                        <?php if (session('msg')) : ?>
                            <?php if (session('msg')) : ?>
                                <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                        <form id="frm-aporte-evaluacion" action="<?= base_url((route_to('rubricas_evaluacion_store'))) ?>" method="post">
                            <div class="form-group <?= session('errors.ru_nombre') ? 'has-error' : '' ?>">
                                <label for="ru_nombre">Nombre:</label>
                                <input type="text" name="ru_nombre" id="ru_nombre" value="<?= old('ru_nombre') ?>" class="form-control" autofocus>
                                <span class="help-block"><?= session('errors.ru_nombre') ?></span>
                            </div>
                            <div class="form-group <?= session('errors.ru_abreviatura') ? 'has-error' : '' ?>">
                                <label for="ru_abreviatura">Abreviatura:</label>
                                <input type="text" name="ru_abreviatura" id="ru_abreviatura" value="<?= old('ru_abreviatura') ?>" class="form-control">
                                <span class="help-block"><?= session('errors.ru_abreviatura') ?></span>
                            </div>
                            <div class="form-group <?= session('errors.id_periodo_evaluacion') ? 'has-error' : '' ?>">
                                <label for="id_periodo_evaluacion">Periodo de Evaluación:</label>
                                <select name="id_periodo_evaluacion" id="id_periodo_evaluacion" class="form-control">
                                    <?php foreach ($periodos_evaluacion as $v) : ?>
                                        <option value="<?= $v->id_periodo_evaluacion ?>" <?= old('id_periodo_evaluacion') == $v->id_periodo_evaluacion ? 'selected' : '' ?>>
                                            <?= $v->pe_nombre ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="help-block"><?= session('errors.id_periodo_evaluacion') ?></span>
                            </div>
                            <div class="form-group <?= session('errors.id_aporte_evaluacion') ? 'has-error' : '' ?>">
                                <label for="id_aporte_evaluacion">Aporte de Evaluación:</label>
                                <select name="id_aporte_evaluacion" id="id_aporte_evaluacion" class="form-control">
                                </select>
                                <span class="help-block"><?= session('errors.id_tipo_aporte') ?></span>
                            </div>
                            <div class="form-group <?= session('errors.id_tipo_asignatura') ? 'has-error' : '' ?>">
                                <label for="id_tipo_asignatura">Tipo de Asignatura:</label>
                                <select name="id_tipo_asignatura" id="id_tipo_asignatura" class="form-control">
                                    <?php foreach ($tipos_asignatura as $v) : ?>
                                        <option value="<?= $v->id_tipo_asignatura ?>" <?= old('id_tipo_asignatura') == $v->id_tipo_asignatura ? 'selected' : '' ?>>
                                            <?= $v->ta_descripcion ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="help-block"><?= session('errors.id_tipo_asignatura') ?></span>
                            </div>
                            <div class="form-group">
                                <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                <a href="<?= base_url(route_to('rubricas_evaluacion')) ?>" class="btn btn-default">Regresar</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>

<?= $this->section('scripts') ?>
<script>
    $(document).ready(function() {
        // JQuery Listo para utilizar
        if ($("#id_periodo_evaluacion").length > 0) {
            cargarAportesEvaluacion();
        }
        $("#id_periodo_evaluacion").change(function() {
            // Código para recuperar los aportes de evaluación asociados al período de evaluación seleccionado
            if ($(this).val() == null) {
                $("#id_aporte_evaluacion").attr("disabled", true);
                $("#id_tipo_asignatura").attr("disabled", true);
                alert('Debes seleccionar un periodo de evaluacion...');
            } else {
                cargarAportesEvaluacion();
                $("#id_aporte_evaluacion").attr("disabled", false);
                $("#id_tipo_asignatura").attr("disabled", false);
            }
        });
    });

    function cargarAportesEvaluacion() {
        var id = $("#id_periodo_evaluacion").val();
        document.getElementById("id_aporte_evaluacion").length = 0;
        $("#btn-save").attr("disabled", true);
        var request = $.ajax({
            url: "<?= base_url(route_to('aportes_getByPeriodoId')) ?>",
            method: "post",
            data: {
                id_periodo: id
            },
            dataType: "json"
        });
        request.done(function(data) {
            var html = '';
            for (var i = 0; i < data.length; i++) {
                html += '<option value="' + data[i].id_aporte_evaluacion + '">' + data[i].ap_nombre + '</option>';
            }
            $("#id_aporte_evaluacion").append(html);
            $("#btn-save").attr("disabled", false);
        });
        request.fail(function(jqXHR, textStatus) {
            alert("Requerimiento fallido: " + jqXHR.responseText);
        });
    }
</script>
<?= $this->endsection('scripts') ?>