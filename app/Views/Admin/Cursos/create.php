<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Cursos
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Cursos
        <small>Crear</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-success">
                        <div id="titulo" class="panel-heading">Nuevo Curso</div>
                    </div>
                    <div class="panel-body">
                        <?php if (session('msg')) : ?>
                            <?php if (session('msg')) : ?>
                                <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                        <form id="frm_curso" class="form-horizontal" action="<?= base_url((route_to('cursos_store'))) ?>" method="post">
                            <div class="form-group <?= session('errors.cu_nombre') ? 'has-error' : '' ?>">
                                <label for="cu_nombre" class="col-sm-2 control-label">Nombre:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="cu_nombre" id="cu_nombre" value="<?= old('cu_nombre') ?>" class="form-control">
                                    <span class="help-block"><?= session('errors.cu_nombre') ?></span>
                                </div>
                            </div>
                            <div class="form-group <?= session('errors.cu_shortname') ? 'has-error' : '' ?>">
                                <label for="cu_shortname" class="col-sm-2 control-label">Nombre Corto:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="cu_shortname" id="cu_shortname" value="<?= old('cu_shortname') ?>" class="form-control">
                                    <span class="help-block"><?= session('errors.cu_shortname') ?></span>
                                </div>
                            </div>
                            <div class="form-group <?= session('errors.cu_abreviatura') ? 'has-error' : '' ?>">
                                <label for="cu_abreviatura" class="col-sm-2 control-label">Abreviatura:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="cu_abreviatura" id="cu_abreviatura" value="<?= old('cu_abreviatura') ?>" class="form-control">
                                    <span class="help-block"><?= session('errors.cu_abreviatura') ?></span>
                                </div>
                            </div>
                            <div class="form-group <?= session('errors.quien_inserta_comp') ? 'has-error' : '' ?>">
                                <label for="quien_inserta_comp" class="col-sm-2 control-label">¿Quién inserta comportamiento?</label>
                                <div class="col-sm-10">
                                    <select name="quien_inserta_comp" id="quien_inserta_comp" class="form-control">
                                        <option value="0" <?= old('quien_inserta_comp') == 0 ? 'selected' : '' ?>>Docente</option>
                                        <option value="1" <?= old('quien_inserta_comp') == 1 ? 'selected' : '' ?>>Tutor</option>
                                    </select>
                                    <span class="help-block"><?= session('errors.quien_inserta_comp') ?></span>
                                </div>
                            </div>
                            <div class="form-group <?= session('errors.id_especialidad') ? 'has-error' : '' ?>">
                                <label for="id_especialidad" class="col-sm-2 control-label">Especialidad:</label>
                                <div class="col-sm-10">
                                    <select name="id_especialidad" id="id_especialidad" class="form-control">
                                        <option value="">Seleccione...</option>
                                        <?php foreach($especialidades as $especialidad): ?>
                                        <option value="<?=$especialidad->id_especialidad;?>" <?= old('id_especialidad') == $especialidad->id_especialidad ? 'selected' : '' ?>><?=$especialidad->es_figura;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <span class="help-block"><?= session('errors.id_especialidad') ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2">
                                </div>
                                <div class="col-sm-10">
                                    <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                    <a href="<?= base_url(route_to('cursos')) ?>" class="btn btn-default">Regresar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>
