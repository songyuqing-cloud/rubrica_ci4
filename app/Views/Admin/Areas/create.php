<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Areas
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Areas
        <small>Crear</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-success">
                        <div id="titulo" class="panel-heading">Nueva Area</div>
                    </div>
                    <div class="panel-body">
                        <?php if (session('msg')) : ?>
                            <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                            </div>
                        <?php endif ?>
                        <form id="frm-area" action="<?= base_url((route_to('areas_store'))) ?>" method="post">
                            <div class="form-group <?= session('errors.ar_nombre') ? 'has-error' : '' ?>">
                                <label for="ar_nombre">Nombre:</label>
                                <input type="text" name="ar_nombre" id="ar_nombre" value="<?= old('ar_nombre') ?>" class="form-control" autofocus>
                                <span class="help-block"><?= session('errors.ar_nombre') ?></span>
                            </div>
                            <div class="form-group">
                                <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                <a href="<?= base_url(route_to('areas')) ?>" class="btn btn-default">Regresar</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>