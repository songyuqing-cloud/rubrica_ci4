<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Usuarios
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Usuarios
        <small>Listado</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12 table-responsive">
                    <?php if (session('msg')) : ?>
                        <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                        </div>
                    <?php endif ?>
                    <a class="btn btn-primary" href="<?= base_url(route_to('usuarios_create')) ?>">Crear Usuario</a>
                    <hr>
                    <table id="t_usuarios" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Avatar</th>
                                <th>Nombre</th>
                                <th>Usuario</th>
                                <th>Activo</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_usuarios">
                            <?php foreach ($usuarios as $v) : ?>
                                <tr>
                                    <td><?= $v->id_usuario ?></td>
                                    <td>
                                        <img class="img-thumbnail" width="50" 
                                             src="<?= base_url() ?>/public/uploads/<?= $v->us_foto ?>" 
                                             alt="Avatar del Usuario">
                                    </td>
                                    <td><?= $v->us_shortname ?></td>
                                    <td><?= $v->us_login ?></td>
                                    <td><?= $v->us_activo == 1 ? 'Sí' : 'No' ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="<?= base_url(route_to('usuarios_edit', $v->id_usuario)) ?>" class="btn btn-warning btn-sm" title="Editar"><span class="fa fa-pencil"></span></a>
                                            <a href="<?= base_url(route_to('usuarios_delete', $v->id_usuario)) ?>" class="btn btn-danger btn-sm" title="Eliminar"><span class="fa fa-remove"></span></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                    <?= $pager->links() ?>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>