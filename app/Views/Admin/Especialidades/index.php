<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Especialidades
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Especialidades
        <small>Listado</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12 table-responsive">
                    <a class="btn btn-primary" href="<?= base_url(route_to('especialidades_create')) ?>">Crear Especialidad</a>
                    <?php if (session('msg')) : ?>
                        <div class="alert alert-<?= session('msg.type') ?> alert-dismissible" style="margin-top: 2px">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                        </div>
                    <?php endif ?>
                    <hr>
                    <table id="t_especialidades" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Figura</th>
                                <th>Nivel de Educación</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_especialidades">
                            <?php foreach ($especialidades as $v) : ?>
                                <tr data-index="<?= $v->id_especialidad ?>" data-orden="<?= $v->es_orden ?>">
                                    <td><?= $v->id_especialidad ?></td>
                                    <td><?= $v->es_nombre ?></td>
                                    <td><?= $v->es_figura ?></td>
                                    <td><?= $v->te_nombre ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="<?= base_url(route_to('especialidades_edit', $v->id_especialidad)) ?>" class="btn btn-warning btn-sm" title="Editar"><span class="fa fa-pencil"></span></a>
                                            <a href="<?= base_url(route_to('especialidades_delete', $v->id_especialidad)) ?>" class="btn btn-danger btn-sm" title="Eliminar"><span class="fa fa-remove"></span></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>

<?= $this->section('scriptsPlugins') ?>
<script src="<?php echo base_url(); ?>/public/jquery-ui/jquery-ui.min.js"></script>
<?= $this->endsection('scriptsPlugins') ?>

<?= $this->section('scripts') ?>
<script>
$(document).ready(function() {

    $('table tbody').sortable({
        update: function(event, ui) {
            $(this).children().each(function(index) {
                if ($(this).attr('data-orden') != (index + 1)) {
                    $(this).attr('data-orden', (index + 1)).addClass('updated');
                }
            });
            saveNewPositions();
        }
    });
});

function saveNewPositions() {
    var positions = [];
    $('.updated').each(function() {
        positions.push([$(this).attr('data-index'), $(this).attr('data-orden')]);
        $(this).removeClass('updated');
    });

    $.ajax({
        url: "<?= base_url(route_to('especialidades_saveNewPositions')) ?>",
        method: 'POST',
        dataType: 'text',
        data: {
            positions: positions
        },
        success: function(response) {
            console.log(response);
        }
    });
}
</script>
<?= $this->endsection('scripts') ?>
