<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Asignaturas
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Asignaturas
        <small>Editar</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-success">
                        <div id="titulo" class="panel-heading">Asignatura: <?= $asignatura->as_nombre ?></div>
                    </div>
                    <div class="panel-body">
                        <?php if (session('msg')) : ?>
                            <?php if (session('msg')) : ?>
                                <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                        <form id="frm-asignatura" action="<?= base_url((route_to('asignaturas_update'))) ?>" method="post">
                            <input type="hidden" name="id_asignatura" value="<?= $asignatura->id_asignatura ?>">
                            <div class="form-group <?= session('errors.as_nombre') ? 'has-error' : '' ?>">
                                <label for="as_nombre">Nombre:</label>
                                <input type="text" name="as_nombre" id="as_nombre" value="<?= old('as_nombre') ?? $asignatura->as_nombre ?>" class="form-control" autofocus>
                                <span class="help-block"><?= session('errors.as_nombre') ?></span>
                            </div>
                            <div class="form-group <?= session('errors.as_abreviatura') ? 'has-error' : '' ?>">
                                <label for="as_abreviatura">Abreviatura:</label>
                                <input type="text" name="as_abreviatura" id="as_abreviatura" value="<?= old('as_abreviatura') ?? $asignatura->as_abreviatura ?>" class="form-control" autofocus>
                                <span class="help-block"><?= session('errors.as_abreviatura') ?></span>
                            </div>
                            <div class="form-group <?= session('errors.id_tipo_asignatura') ? 'has-error' : '' ?>">
                                <label for="id_tipo_asignatura">Tipo de Asignatura:</label>
                                <select name="id_tipo_asignatura" id="id_tipo_asignatura" class="form-control">
                                <?php foreach($tipos_asignatura as $tipo_asignatura): ?>
                                    <?php
                                        $selected = '';
                                        if(!empty(old('id_tipo_asignatura'))){
                                            if(old('id_tipo_asignatura') == $tipo_asignatura->id_tipo_asignatura){
                                                $selected = 'selected';
                                            }
                                        } else {
                                            if($asignatura->id_tipo_asignatura == $tipo_asignatura->id_tipo_asignatura){
                                                $selected = 'selected';
                                            }
                                        }
                                    ?>
                                    <option value="<?= $tipo_asignatura->id_tipo_asignatura; ?>" <?= $selected ?>>
                                        <?= $tipo_asignatura->ta_descripcion; ?>
                                    </option>
                                <?php endforeach; ?>
                                </select>
                                <span class="help-block"><?= session('errors.id_tipo_asignatura') ?></span>
                            </div>
                            <div class="form-group <?= session('errors.id_area') ? 'has-error' : '' ?>">
                                <label for="id_area">Areas:</label>
                                <select name="id_area" id="id_area" class="form-control">
                                <?php foreach($areas as $area): ?>
                                    <?php
                                        $selected = '';
                                        if(!empty(old('id_area'))){
                                            if(old('id_area') == $area->id_area){
                                                $selected = 'selected';
                                            }
                                        } else {
                                            if($asignatura->id_area == $area->id_area){
                                                $selected = 'selected';
                                            }
                                        }
                                    ?>
                                    <option value="<?= $area->id_area; ?>" <?= $selected ?>>
                                        <?= $area->ar_nombre; ?>
                                    </option>
                                <?php endforeach; ?>
                                </select>
                                <span class="help-block"><?= session('errors.id_area') ?></span>
                            </div>
                            <div class="form-group">
                                <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                <a href="<?= base_url(route_to('asignaturas')) ?>" class="btn btn-default">Regresar</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>