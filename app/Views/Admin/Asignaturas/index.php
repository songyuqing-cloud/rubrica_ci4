<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Asignaturas
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Asignaturas
        <small>Listado</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12 table-responsive">
                    <?php if (session('msg')) : ?>
                        <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                        </div>
                    <?php endif ?>
                    <a class="btn btn-primary" href="<?= base_url(route_to('asignaturas_create')) ?>">Crear Asignatura</a>
                    <hr>
                    <table id="t_asignaturas" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Area</th>
                                <th>Nombre</th>
                                <th>Abreviatura</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="tbody_asignaturas">
                            <?php foreach ($asignaturas as $v) : ?>
                                <tr>
                                    <td><?= $v->id_asignatura ?></td>
                                    <td><?= $v->ar_nombre ?></td>
                                    <td><?= $v->as_nombre ?></td>
                                    <td><?= $v->as_abreviatura ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="<?= base_url(route_to('asignaturas_edit', $v->id_asignatura)) ?>" class="btn btn-warning btn-sm" title="Editar"><span class="fa fa-pencil"></span></a>
                                            <a href="<?= base_url(route_to('asignaturas_delete', $v->id_asignatura)) ?>" class="btn btn-danger btn-sm" title="Eliminar"><span class="fa fa-remove"></span></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                    <?= $pager->links() ?>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>