<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Periodos de Evaluación
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Periodos de Evaluación
        <small>Crear</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-success">
                        <div id="titulo" class="panel-heading">Nuevo Periodo de Evaluación</div>
                    </div>
                    <div class="panel-body">
                        <?php if (session('msg')) : ?>
                            <?php if (session('msg')) : ?>
                                <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                        <form id="frm_periodo_evaluacion" class="form-horizontal" action="<?= base_url((route_to('periodos_evaluacion_store'))) ?>" method="post">
                            <div class="form-group <?= session('errors.pe_nombre') ? 'has-error' : '' ?>">
                                <label for="pe_nombre" class="col-sm-2 control-label">Nombre:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="pe_nombre" id="pe_nombre" value="<?= old('pe_nombre') ?>" class="form-control">
                                    <span class="help-block"><?= session('errors.pe_nombre') ?></span>
                                </div>
                            </div>
                            <div class="form-group <?= session('errors.pe_abreviatura') ? 'has-error' : '' ?>">
                                <label for="pe_abreviatura" class="col-sm-2 control-label">Abreviatura:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="pe_abreviatura" id="pe_abreviatura" value="<?= old('pe_abreviatura') ?>" class="form-control">
                                    <span class="help-block"><?= session('errors.pe_abreviatura') ?></span>
                                </div>
                            </div>
                            <div class="form-group <?= session('errors.id_tipo_periodo') ? 'has-error' : '' ?>">
                                <label for="id_tipo_periodo" class="col-sm-2 control-label">Tipo Periodo:</label>
                                <div class="col-sm-10">
                                    <select name="id_tipo_periodo" id="id_tipo_periodo" class="form-control">
                                        <option value="">Seleccione...</option>
                                        <?php foreach($tipos_periodo as $tipo_periodo): ?>
                                        <option value="<?=$tipo_periodo->id_tipo_periodo;?>" <?= old('id_tipo_periodo') == $tipo_periodo->id_tipo_periodo ? 'selected' : '' ?>><?=$tipo_periodo->tp_descripcion;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <span class="help-block"><?= session('errors.id_tipo_periodo') ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2">
                                </div>
                                <div class="col-sm-10">
                                    <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                    <a href="<?= base_url(route_to('periodos_evaluacion')) ?>" class="btn btn-default">Regresar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>
