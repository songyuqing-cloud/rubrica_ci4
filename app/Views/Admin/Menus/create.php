<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Menus
<?= $this->endsection('titulo') ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>/public/css/custom.css">
<?= $this->endsection('styles') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Menus
        <small>Crear</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-success">
                        <div id="titulo" class="panel-heading">Nuevo Menú</div>
                    </div>
                    <div class="panel-body">
                        <?php if (session('msg')) : ?>
                            <?php if (session('msg')) : ?>
                                <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                        <form id="frm-menu" action="<?= base_url((route_to('menus_store'))) ?>" method="post" autocomplete="off">
                            <div class="row">
                                <div class="col-lg-11 col-md-11 col-sm-11">
                                    <div class="form-group <?= session('errors.id_perfil') ? 'has-error' : '' ?>">
                                        <label for="id_perfil" class="requerido">Perfil:</label>
                                        <select name="id_perfil" id="id_perfil" class="form-control">
                                            <?php foreach ($perfiles as $perfil) : ?>
                                                <option value="<?php echo $perfil->id_perfil ?>">
                                                    <?php echo $perfil->pe_nombre; ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-11 col-md-11 col-sm-11">
                                    <div class="form-group <?= session('errors.mnu_texto') ? 'has-error' : '' ?>">
                                        <label for="mnu_texto" class="requerido">Texto:</label>
                                        <input type="text" name="mnu_texto" id="mnu_texto" value="<?= old('mnu_texto') ?>" class="form-control" autofocus>
                                        <span class="help-block"><?= session('errors.mnu_texto') ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-11 col-md-11 col-sm-11">
                                    <div class="form-group <?= session('errors.mnu_link') ? 'has-error' : '' ?>">
                                        <label for="mnu_link" class="requerido">Enlace:</label>
                                        <input type="text" name="mnu_link" id="mnu_link" value="<?= old('mnu_link') ?>" class="form-control" autofocus>
                                        <span class="help-block"><?= session('errors.mnu_link') ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-11 col-md-11 col-sm-11">
                                    <div class="form-group <?= session('errors.mnu_icono') ? 'has-error' : '' ?>">
                                        <label for="mnu_icono">Icono:</label>
                                        <input type="text" name="mnu_icono" id="mnu_icono" value="<?= old('mnu_icono') ?>" class="form-control">
                                        <span class="help-block"><?= session('errors.mnu_icono') ?></span>
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1">
                                    <span id="mostrar-icono" class="fa fa-fw <?= old('mnu_icono') ?>"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-11 col-md-11 col-sm-11">
                                    <div class="form-group">
                                        <label for="mnu_publicado" class="requerido">Publicado:</label>
                                        <select name="mnu_publicado" id="mnu_publicado" class="form-control">
                                            <option value="1">Sí</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                <a href="<?= base_url(route_to('menus')) ?>" class="btn btn-default">Regresar</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>

<?= $this->section('scripts') ?>
<script>
    $(document).ready(function() {
        $('#mnu_icono').on('blur', function() {
            $('#mostrar-icono').removeClass().addClass('fa fa-fw ' + $(this).val());
        });
    });
</script>
<?= $this->endsection('scripts') ?>