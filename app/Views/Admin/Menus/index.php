<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Menus
<?= $this->endsection('titulo') ?>

<?= $this->section('scriptsPlugins') ?>
<script src="<?php echo base_url(); ?>/public/jquery-nestable/jquery.nestable.js"></script>
<?= $this->endsection('scriptsPlugins') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 id="titulo_principal">
        Menus
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 table-responsive">
                    <?php if (session('msg')) : ?>
                    <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                    </div>
                    <?php endif ?>
                    <!-- Aquí va el listado de menús -->
                    <select name="cboPerfiles" id="cboPerfiles" class="form-control">
                        <option value="0">Seleccione un perfil...</option>
                        <?php foreach ($perfiles as $perfil) : ?>
                        <option value="<?php echo $perfil->id_perfil ?>">
                            <?php echo $perfil->pe_nombre; ?>
                        </option>
                        <?php endforeach; ?>
                    </select>
                    <hr>
                    <table id="example1" class="table table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Texto</th>
                                <th>Publicado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_menus">
                            <!-- En este lugar se van a poblar los menús mediante AJAX -->
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="panel panel-success">
                        <div id="titulo" class="panel-heading">Nuevo Menu</div>
                    </div>
                    <div class="panel-body">
                        <form id="frm-menu" action="" method="post">
                            <input type="hidden" name="id_menu" id="id_menu" value="0">
                            <input type="hidden" name="mnu_padre" id="mnu_padre" value="0">
                            <input type="hidden" name="mnu_nivel" id="mnu_nivel" value="1">
                            <div class="form-group">
                                <label for="mnu_texto">Texto:</label>
                                <input type="text" name="mnu_texto" id="mnu_texto" class="form-control" autofocus>
                            </div>
                            <div class="form-group">
                                <label for="mnu_link">Enlace:</label>
                                <input type="text" name="mnu_link" id="mnu_link" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="mnu_publicado">Publicado:</label>
                                <select name="mnu_publicado" id="mnu_publicado" class="form-control">
                                    <option value="1">Sí</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                <button id="btn-cancel" type="button" class="btn btn-info">Cancelar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="btn-regresar" class="col-lg-12 col-md-12 col-sm-12">
                    <!-- Aquí va el botón para regresar al listado de menús -->
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>

<?= $this->section('scriptsPlugins') ?>
<script src="<?php echo base_url(); ?>/public/jquery-ui/jquery-ui.min.js"></script>
<?= $this->endsection('scriptsPlugins') ?>

<?= $this->section('scripts') ?>
<script src="<?php echo base_url(); ?>/public/js/funciones.js"></script>

<script>
$(document).ready(function() {
    $("#tbody_menus").html("<tr><td colspan='4' align='center'>Debe seleccionar un perfil...</td></tr>");

    $("#cboPerfiles").change(function() {
        var id_perfil = $(this).val();
        if (id_perfil == 0)
            $("#tbody_menus").html(
                "<tr><td colspan='4' align='center'>Debe seleccionar un perfil...</td></tr>");
        else
            showMenusPerfil(id_perfil);
    });

    $('table tbody').sortable({
        update: function(event, ui) {
            $(this).children().each(function(index) {
                if ($(this).attr('data-orden') != (index + 1)) {
                    $(this).attr('data-orden', (index + 1)).addClass('updated');
                }
            });
            saveNewPositions();
        }
    });

    $('#tbody_menus').on('click', '.item-submenus', function() {
        var id_menu = $(this).attr('data');
        var mnu_texto = $(this).attr('data-mnu-texto');
        $("#id_menu").val(id_menu);
        $("#mnu_padre").val(id_menu);
        $("#mnu_nivel").val("2");
        $("#titulo_principal").html("SubMenus: " + mnu_texto);
        $("#cboPerfiles").prop("disabled", true);
        $("#titulo").html("Nuevo SubMenu");
        $("#btn-save").html("Guardar");
        showSubMenus(id_menu);
        $("#btn-regresar").html('<a id="back_to_menus" onclick="regresar()" href="javascript:;" class="btn btn-primary">Regresar a los menus</a>');
        $("#frm-menu")[0].reset();
        $("#mnu_texto").focus();
    });

    $('#tbody_menus').on('click', '.item-edit', function(){
        var id_menu = $(this).attr('data');
        var mnu_padre = $("#mnu_padre").val();
        if(mnu_padre==0)
            $("#titulo").html("Editar Menu");
        else
            $("#titulo").html("Editar SubMenu");
        $("#btn-save").html("Actualizar");
        $.ajax({
            url: "<?= base_url(route_to('menus_getById')) ?>",
            method: "post",
            data: {
                id_menu: id_menu
            },
            dataType: "json",
            success: function(data){
                $("#id_menu").val(id_menu);
                $("#mnu_texto").val(data.mnu_texto);
                $("#mnu_link").val(data.mnu_link);
                setearIndice('mnu_publicado', data.mnu_publicado);
            },
            error: function(jqXHR, textStatus){
                alert(jqXHR.responseText);
            }
        });
    });
});

$("#btn-cancel").click(function(){
    var mnu_padre = $("#mnu_padre").val();
    $("#frm-menu")[0].reset();
    if(mnu_padre==0) 
        $("#titulo").html("Nuevo Menu");
    else 
        $("#titulo").html("Nuevo SubMenu");
    $("#btn-save").html("Guardar");
    $("#mnu_texto").focus();
});

function showSubMenus(id_menu) {
    var request = $.ajax({
        url: "<?= base_url(route_to('menus_getByParentId')) ?>",
        method: "post",
        data: {
            id_menu: id_menu
        },
        dataType: "json"
    });

    request.done(function(data) {
        var html = '';
        var i, publicado;
        for (i = 0; i < data.length; i++) {
            publicado = data[i].mnu_publicado == 1 ? 'Sí' : 'No';
            html += '<tr data-index="' + data[i].id_menu + '" data-orden="' + data[i].mnu_orden + '">' +
                '<td>' + data[i].id_menu + '</td>' +
                '<td>' + data[i].mnu_texto + '</td>' +
                '<td>' + publicado + '</td>' +
                '<td>' +
                '<div class="btn-group">' +
                '<a href="javascript:;" class="btn btn-warning item-edit" data="' + data[i].id_menu +
                '" title="Editar"><span class="fa fa-pencil"></span></a>' +
                '<a href="javascript:;" class="btn btn-danger item-delete" data="' + data[i].id_menu +
                '" title="Eliminar"><span class="fa fa-remove"></span></a>' +
                '</div>' +
                '</td>' +
                '</tr>';
        }
        $("#tbody_menus").html(html);
    });

    request.fail(function(jqXHR, textStatus) {
        alert("Requerimiento fallido: " + jqXHR.responseText);
    });
}

function regresar() {
    var id_perfil = $("#cboPerfiles").val();
    $("#btn-regresar").html('');
    $("#titulo_principal").html("Menús");
    $("#cboPerfiles").prop("disabled", false);
    showMenusPerfil(id_perfil);
}

function saveNewPositions() {
    var positions = [];
    $('.updated').each(function() {
        positions.push([$(this).attr('data-index'), $(this).attr('data-orden')]);
        $(this).removeClass('updated');
    });

    $.ajax({
        url: "<?= base_url(route_to('menus_saveNewPositions')) ?>",
        method: 'POST',
        dataType: 'text',
        data: {
            positions: positions
        },
        success: function(response) {
            console.log(response);
        }
    });
}

function showMenusPerfil(id_perfil) {
    var request = $.ajax({
        url: "<?= base_url(route_to('menus_getByRoleId')) ?>",
        method: "post",
        data: {
            id_perfil: id_perfil
        },
        dataType: "json"
    });

    request.done(function(data) {
        var html = '';
        var i, publicado;
        for (i = 0; i < data.length; i++) {
            publicado = data[i].mnu_publicado == 1 ? 'Sí' : 'No';
            html += '<tr data-index="' + data[i].id_menu + '" data-orden="' + data[i].mnu_orden + '">' +
                '<td>' + data[i].id_menu + '</td>' +
                '<td>' + data[i].mnu_texto + '</td>' +
                '<td>' + publicado + '</td>' +
                '<td>' +
                '<div class="btn-group">' +
                '<a href="javascript:;" class="btn btn-success item-submenus" data="' + data[i].id_menu +
                '" data-mnu-texto="' + data[i].mnu_texto +
                '" title="Submenús"><span class="fa fa-list"></span></a>' +
                '<a href="javascript:;" class="btn btn-warning item-edit" data="' + data[i].id_menu +
                '" title="Editar"><span class="fa fa-pencil"></span></a>' +
                '<a href="javascript:;" class="btn btn-danger item-delete" data="' + data[i].id_menu +
                '" title="Eliminar"><span class="fa fa-remove"></span></a>' +
                '</div>' +
                '</td>' +
                '</tr>';
        }
        $("#tbody_menus").html(html);
    });

    request.fail(function(jqXHR, textStatus) {
        alert("Requerimiento fallido: " + jqXHR.responseText);
    });
}

$("#frm-menu").submit(function(e) {
    e.preventDefault();
    var url;
    var id_menu = $("#id_menu").val();
    var base_url = $("#base_url").val();
    var id_perfil = $("#cboPerfiles").val();
    var mnu_texto = $.trim($("#mnu_texto").val());
    var mnu_link = $.trim($("#mnu_link").val());
    var mnu_publicado = $("#mnu_publicado").val();
    var mnu_padre = $("#mnu_padre").val();
    var mnu_nivel = $("#mnu_nivel").val();

    if (id_perfil == 0) {
        swal("Ocurrió un error inesperado!", "Debe seleccionar un perfil.", "error");
    } else if (mnu_texto == "") {
        swal("Ocurrió un error inesperado!", "Debe ingresar el texto del menú.", "error");
    } else if (mnu_link == "") {
        swal("Ocurrió un error inesperado!", "Debe ingresar el link del menú.", "error");
    } else {

        if ($("#btn-save").html() == "Guardar")
            url = "<?= base_url(route_to('menus_store')) ?>";
        else if ($("#btn-save").html() == "Actualizar")
            url = "<?= base_url(route_to('menus_update')) ?>";

        $.ajax({
            url: url,
            method: "post",
            data: {
                id_menu: id_menu,
                id_perfil: id_perfil,
                mnu_texto: mnu_texto,
                mnu_link: mnu_link,
                mnu_publicado: mnu_publicado,
                mnu_padre: mnu_padre,
                mnu_nivel: mnu_nivel
            },
            dataType: "json",
            success: function(response) {

                swal({
                    title: response.titulo,
                    text: response.mensaje,
                    type: response.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });

                if (mnu_padre == 0) {
                    showMenusPerfil(id_perfil);
                } else {
                    showSubMenus(mnu_padre);
                }

                $("#frm-menu")[0].reset();

                if ($("#btn-save").html() == "Actualizar") {
                    $("#btn-save").html("Guardar");
                    if(mnu_padre==0){
                        $("#titulo").html("Nuevo Menu");
                    }else{
                        $("#titulo").html("Nuevo SubMenu");
                    }
                }

                $("#mnu_texto").focus();

            },
            error: function(jqXHR, textStatus) {
                alert(jqXHR.responseText);
            }
        });
    }
});
</script>
<?= $this->endsection('scripts') ?>