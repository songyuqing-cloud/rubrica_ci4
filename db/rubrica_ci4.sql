-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-07-2021 a las 08:22:57
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `rubrica_ci4`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `version` varchar(255) NOT NULL,
  `class` text NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `version`, `class`, `group`, `namespace`, `time`, `batch`) VALUES
(62, '2021-02-28-001653', 'App\\Database\\Migrations\\SwUsuario', 'default', 'App', 1624737460, 1),
(63, '2021-02-28-015909', 'App\\Database\\Migrations\\SwPerfil', 'default', 'App', 1624737460, 1),
(64, '2021-02-28-021627', 'App\\Database\\Migrations\\SwUsuarioPerfil', 'default', 'App', 1624737460, 1),
(65, '2021-02-28-115231', 'App\\Database\\Migrations\\SwPeriodoEstado', 'default', 'App', 1624737460, 1),
(66, '2021-02-28-115605', 'App\\Database\\Migrations\\SwPeriodoLectivo', 'default', 'App', 1624737460, 1),
(67, '2021-02-28-121938', 'App\\Database\\Migrations\\SwInstitucion', 'default', 'App', 1624737460, 1),
(68, '2021-03-05-120352', 'App\\Database\\Migrations\\SwMenu', 'default', 'App', 1624737460, 1),
(69, '2021-03-05-131213', 'App\\Database\\Migrations\\SwMenuPerfil', 'default', 'App', 1624737460, 1),
(70, '2021-03-13-110840', 'App\\Database\\Migrations\\SwModalidad', 'default', 'App', 1624737460, 1),
(71, '2021-06-11-161524', 'App\\Database\\Migrations\\SwTipoEducacion', 'default', 'App', 1624737460, 1),
(72, '2021-06-12-150407', 'App\\Database\\Migrations\\SwEspecialidad', 'default', 'App', 1624737460, 1),
(73, '2021-06-14-143336', 'App\\Database\\Migrations\\SwCurso', 'default', 'App', 1624737460, 1),
(74, '2021-06-16-133510', 'App\\Database\\Migrations\\SwJornada', 'default', 'App', 1624737460, 1),
(75, '2021-06-16-140624', 'App\\Database\\Migrations\\SwParalelo', 'default', 'App', 1624737460, 1),
(76, '2021-06-17-144118', 'App\\Database\\Migrations\\SwArea', 'default', 'App', 1624737460, 1),
(77, '2021-06-17-223351', 'App\\Database\\Migrations\\SwTipoAsignatura', 'default', 'App', 1624737460, 1),
(78, '2021-06-18-003625', 'App\\Database\\Migrations\\SwAsignatura', 'default', 'App', 1624737460, 1),
(79, '2021-06-23-144722', 'App\\Database\\Migrations\\SwTipoPeriodo', 'default', 'App', 1624737460, 1),
(80, '2021-06-24-112657', 'App\\Database\\Migrations\\SwPeriodoEvaluacion', 'default', 'App', 1624737460, 1),
(81, '2021-06-27-143916', 'App\\Database\\Migrations\\SwTipoAporte', 'default', 'App', 1624804903, 2),
(82, '2021-06-27-194523', 'App\\Database\\Migrations\\SwAporteEvaluacion', 'default', 'App', 1624837821, 3),
(85, '2021-07-17-160343', 'App\\Database\\Migrations\\SwRubricaEvaluacion', 'default', 'App', 1626544515, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_aporte_evaluacion`
--

CREATE TABLE `sw_aporte_evaluacion` (
  `id_aporte_evaluacion` int(11) UNSIGNED NOT NULL,
  `id_periodo_evaluacion` int(11) UNSIGNED NOT NULL,
  `id_tipo_aporte` int(11) UNSIGNED NOT NULL,
  `ap_nombre` varchar(24) NOT NULL,
  `ap_shortname` varchar(45) NOT NULL,
  `ap_abreviatura` varchar(8) NOT NULL,
  `ap_fecha_apertura` date NOT NULL,
  `ap_fecha_cierre` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_aporte_evaluacion`
--

INSERT INTO `sw_aporte_evaluacion` (`id_aporte_evaluacion`, `id_periodo_evaluacion`, `id_tipo_aporte`, `ap_nombre`, `ap_shortname`, `ap_abreviatura`, `ap_fecha_apertura`, `ap_fecha_cierre`) VALUES
(1, 6, 1, 'PRIMER PARCIAL', '', '1ER.P.', '2020-09-01', '2020-11-06'),
(2, 6, 1, 'SEGUNDO PARCIAL', '', '2DO.P.', '2020-11-09', '2021-01-15'),
(3, 6, 2, 'PROYECTO QUIMESTRAL', '', 'PROY.', '2021-01-18', '2021-01-29'),
(4, 7, 1, 'TERCER PARCIAL', '', '3ER.P.', '2021-02-03', '2021-04-09'),
(5, 7, 1, 'CUARTO PARCIAL', '', '4TO.P.', '2021-04-12', '2021-06-18'),
(6, 7, 2, 'PROYECTO QUIMESTRAL', '', 'PROY.', '2021-06-21', '2021-07-02'),
(7, 8, 3, 'EXAMEN SUPLETORIO', '', 'SUP.', '2021-07-19', '2021-07-23'),
(8, 9, 3, 'EXAMEN REMEDIAL', '', 'REM.', '2021-08-23', '2021-08-27'),
(9, 10, 3, 'EXAMEN DE GRACIA', '', 'GRA.', '2021-08-30', '2021-08-31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_area`
--

CREATE TABLE `sw_area` (
  `id_area` int(11) UNSIGNED NOT NULL,
  `ar_nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_area`
--

INSERT INTO `sw_area` (`id_area`, `ar_nombre`) VALUES
(1, 'CIENCIAS NATURALES'),
(2, 'CIENCIAS SOCIALES'),
(3, 'EDUCACION CULTURAL Y ARTISTICA'),
(4, 'EDUCACION FISICA'),
(5, 'LENGUA EXTRANJERA'),
(6, 'LENGUA Y LITERATURA'),
(7, 'MATEMATICA'),
(8, 'MODULO INTER-ÁREAS'),
(9, 'PROYECTOS ESCOLARES'),
(10, 'CONTABILIDAD'),
(11, 'INFORMATICA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_asignatura`
--

CREATE TABLE `sw_asignatura` (
  `id_asignatura` int(11) UNSIGNED NOT NULL,
  `id_area` int(11) UNSIGNED NOT NULL,
  `id_tipo_asignatura` int(11) UNSIGNED NOT NULL,
  `as_nombre` varchar(84) NOT NULL,
  `as_abreviatura` varchar(12) NOT NULL,
  `as_shortname` varchar(45) NOT NULL,
  `as_curricular` int(1) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_asignatura`
--

INSERT INTO `sw_asignatura` (`id_asignatura`, `id_area`, `id_tipo_asignatura`, `as_nombre`, `as_abreviatura`, `as_shortname`, `as_curricular`) VALUES
(1, 1, 1, 'BIOLOGIA', 'BIO', '', 1),
(2, 1, 1, 'CIENCIAS NATURALES', 'CCNN', '', 1),
(3, 1, 1, 'FISICA', 'FIS', '', 1),
(4, 1, 1, 'QUIMICA', 'QUIM', '', 1),
(5, 2, 1, 'EDUCACION PARA LA CIUDADANIA', 'EDU.C.', '', 1),
(6, 2, 1, 'ESTUDIOS SOCIALES', 'EESS', '', 1),
(7, 2, 1, 'FILOSOFIA', 'FILO', '', 1),
(8, 2, 1, 'HISTORIA', 'HIST', '', 1),
(9, 10, 1, 'CONTABILIDAD BANCARIA', 'CON.BAN.', '', 1),
(10, 10, 1, 'CONTABILIDAD DE COSTOS', 'COSTOS', '', 1),
(11, 10, 1, 'CONTABILIDAD GENERAL Y TESORERIA', 'CONTA', '', 1),
(12, 10, 2, 'FORMACION EN CENTROS DE TRABAJO', 'FCT', '', 1),
(13, 10, 1, 'FORMACION Y ORIENTACION LABORAL', 'FOL', '', 1),
(14, 10, 1, 'GESTION DEL TALENTO HUMANO', 'G.TAL.H.', '', 1),
(15, 10, 1, 'PAQUETES CONTABLES Y TRIBUTARIOS', 'PAQ.CON.', '', 1),
(16, 10, 1, 'TRIBUTACION', 'TRIB.', '', 1),
(17, 3, 1, 'EDUCACION CULTURAL Y ARTISTICA', 'ECA', '', 1),
(18, 4, 1, 'EDUCACION FISICA', 'EEFF', '', 1),
(19, 11, 1, 'APLICACIONES OFIMATICAS LOCALES Y EN LINEA', 'APL.OF.', '', 1),
(20, 11, 1, 'DISEÑO Y DESARROLLO WEB', 'DIS.WEB', '', 1),
(21, 11, 2, 'FORMACION EN CENTROS DE TRABAJO', 'FCT', '', 1),
(22, 11, 1, 'FORMACION Y ORIENTACION LABORAL', 'FOL', '', 1),
(23, 11, 1, 'PROGRAMACION Y BASES DE DATOS', 'PROG.', '', 1),
(24, 11, 1, 'SISTEMAS OPERATIVOS Y REDES', 'SIS.OP.', '', 1),
(25, 11, 1, 'SOPORTE TECNICO', 'SOP.TEC.', '', 1),
(26, 5, 1, 'INGLES', 'ING', '', 1),
(27, 6, 1, 'LENGUA Y LITERATURA', 'LENGUA', '', 1),
(28, 7, 1, 'MATEMATICA', 'MATE', '', 1),
(29, 8, 1, 'EMPRENDIMIENTO Y GESTION', 'EMPRE', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_curso`
--

CREATE TABLE `sw_curso` (
  `id_curso` int(11) UNSIGNED NOT NULL,
  `id_especialidad` int(11) UNSIGNED NOT NULL,
  `id_curso_superior` int(11) UNSIGNED NOT NULL,
  `cu_nombre` varchar(128) NOT NULL,
  `cu_shortname` varchar(45) NOT NULL,
  `cu_abreviatura` varchar(5) NOT NULL,
  `cu_orden` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `quien_inserta_comp` int(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_curso`
--

INSERT INTO `sw_curso` (`id_curso`, `id_especialidad`, `id_curso_superior`, `cu_nombre`, `cu_shortname`, `cu_abreviatura`, `cu_orden`, `quien_inserta_comp`) VALUES
(1, 4, 0, 'OCTAVO', 'OCTAVO', '8vo.', 1, 0),
(2, 4, 0, 'NOVENO', 'NOVENO', '9no.', 2, 0),
(3, 4, 0, 'DECIMO', 'DECIMO', '10mo.', 3, 0),
(4, 5, 0, 'PRIMER CURSO', 'PRIMERO CIENCIAS', '1ero.', 4, 0),
(5, 5, 0, 'SEGUNDO CURSO', 'SEGUNDO CIENCIAS', '2do.', 5, 0),
(6, 5, 0, 'TERCER CURSO', 'TERCERO CIENCIAS', '3ro.', 6, 0),
(7, 6, 0, 'PRIMER CURSO', 'PRIMERO CONTABILIDAD', '1ero.', 7, 0),
(8, 6, 0, 'SEGUNDO CURSO', 'SEGUNDO CONTABILIDAD', '2do.', 8, 0),
(9, 6, 0, 'TERCER CURSO', 'TERCERO CONTABILIDAD', '3ro.', 9, 0),
(10, 7, 0, 'PRIMER CURSO', 'PRIMERO INFORMATICA', '1ero.', 10, 0),
(11, 7, 0, 'SEGUNDO CURSO', 'SEGUNDO INFORMATICA', '2do.', 11, 0),
(12, 7, 0, 'TERCER CURSO', 'TERCERO INFORMATICA', '3ro.', 12, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_especialidad`
--

CREATE TABLE `sw_especialidad` (
  `id_especialidad` int(11) UNSIGNED NOT NULL,
  `id_tipo_educacion` int(11) UNSIGNED NOT NULL,
  `es_nombre` varchar(64) NOT NULL,
  `es_figura` varchar(50) NOT NULL,
  `es_abreviatura` varchar(15) NOT NULL,
  `es_orden` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_especialidad`
--

INSERT INTO `sw_especialidad` (`id_especialidad`, `id_tipo_educacion`, `es_nombre`, `es_figura`, `es_abreviatura`, `es_orden`) VALUES
(1, 1, 'Primer Año', 'Primer Año', 'Pre.', 1),
(2, 2, 'EGB ELEMENTAL', 'EGB ELEMENTAL', 'EGB. 2-3-4', 2),
(3, 3, 'EGB MEDIA', 'EGB MEDIA', 'EGB. 5-6-7', 3),
(4, 4, 'EGB SUPERIOR', 'EDUCACION GENERAL BASICA SUPERIOR', 'EGBS', 4),
(5, 5, 'BACHILLERATO GENERAL UNIFICADO', 'BACHILLERATO GENERAL UNIFICADO', 'BGU', 5),
(6, 6, 'Área Técnica de Servicios', 'CONTABILIDAD', 'CONTA', 6),
(7, 6, 'Área Técnica de Servicios', 'INFORMATICA', 'INFO', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_institucion`
--

CREATE TABLE `sw_institucion` (
  `id_institucion` int(11) UNSIGNED NOT NULL,
  `in_nombre` varchar(64) NOT NULL,
  `in_direccion` varchar(64) NOT NULL,
  `in_telefono` varchar(64) NOT NULL,
  `in_nom_rector` varchar(64) NOT NULL,
  `in_nom_vicerrector` varchar(64) NOT NULL,
  `in_nom_secretario` varchar(64) NOT NULL,
  `in_url` varchar(64) NOT NULL,
  `in_logo` varchar(64) NOT NULL,
  `in_amie` varchar(16) NOT NULL,
  `in_ciudad` varchar(64) NOT NULL,
  `in_copiar_y_pegar` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_institucion`
--

INSERT INTO `sw_institucion` (`id_institucion`, `in_nombre`, `in_direccion`, `in_telefono`, `in_nom_rector`, `in_nom_vicerrector`, `in_nom_secretario`, `in_url`, `in_logo`, `in_amie`, `in_ciudad`, `in_copiar_y_pegar`) VALUES
(1, 'UNIDAD EDUCATIVA PCEI FISCAL SALAMANCA', 'Calle el Tiempo y Pasaje Mónaco', '2256311/2254818', 'DR. RAMIRO CASTILLO', 'Lic. Rómulo Mejía', 'Lic. Alicia Salazar O.', 'http://colegionocturnosalamanca.com', 'logo_salamanca.gif', '17H00215', 'Quito D.M.', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_jornada`
--

CREATE TABLE `sw_jornada` (
  `id_jornada` int(11) UNSIGNED NOT NULL,
  `jo_nombre` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_jornada`
--

INSERT INTO `sw_jornada` (`id_jornada`, `jo_nombre`) VALUES
(1, 'MATUTINA'),
(2, 'VESPERTINA'),
(3, 'NOCTURNA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_menu`
--

CREATE TABLE `sw_menu` (
  `id_menu` int(11) UNSIGNED NOT NULL,
  `mnu_texto` varchar(32) NOT NULL,
  `mnu_link` varchar(64) NOT NULL,
  `mnu_nivel` int(11) UNSIGNED NOT NULL,
  `mnu_orden` int(11) UNSIGNED NOT NULL,
  `mnu_padre` int(11) UNSIGNED NOT NULL,
  `mnu_publicado` int(11) UNSIGNED NOT NULL,
  `mnu_icono` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_menu`
--

INSERT INTO `sw_menu` (`id_menu`, `mnu_texto`, `mnu_link`, `mnu_nivel`, `mnu_orden`, `mnu_padre`, `mnu_publicado`, `mnu_icono`) VALUES
(1, 'Administración', '#', 1, 1, 0, 1, ''),
(2, 'Definiciones', '#', 1, 2, 0, 1, ''),
(3, 'Especificaciones', '#', 1, 3, 0, 1, ''),
(4, 'Asociar', '#', 1, 4, 0, 1, ''),
(5, 'Cierres', '#', 1, 5, 0, 1, ''),
(6, 'Modalidades', '/admin/modalidades', 2, 1, 1, 1, ''),
(7, 'Períodos Lectivos', '/admin/periodos_lectivos', 2, 2, 1, 1, ''),
(8, 'Perfiles', '/admin/perfiles', 2, 3, 1, 1, ''),
(9, 'Menús', '/admin/menus', 2, 4, 1, 1, ''),
(10, 'Usuarios', '/admin/usuarios', 2, 5, 1, 1, ''),
(11, 'Niveles de Educación', '/admin/tipos_educacion', 2, 1, 2, 1, ''),
(12, 'Especialidades', '/admin/especialidades', 2, 2, 2, 1, ''),
(13, 'Cursos', '/admin/cursos', 2, 3, 2, 1, ''),
(14, 'Paralelos', '/admin/paralelos', 2, 4, 2, 1, ''),
(15, 'Areas', '/admin/areas', 2, 5, 2, 1, ''),
(16, 'Asignaturas', '/admin/asignaturas', 2, 6, 2, 1, ''),
(17, 'Institución', '/admin/institucion', 2, 7, 2, 1, ''),
(18, 'Períodos de Evaluación', '/admin/periodos_evaluacion', 2, 1, 3, 1, ''),
(19, 'Aportes de Evaluación', '/admin/aportes_evaluacion', 2, 2, 3, 1, ''),
(20, 'Insumos de Evaluación', '/admin/rubricas_evaluacion', 2, 3, 3, 1, ''),
(21, 'Escalas de Calificaciones', '/admin/escalas_calificaciones', 2, 4, 3, 1, ''),
(22, 'Asignaturas Cursos', '/admin/asignaturas_cursos', 2, 1, 4, 1, ''),
(23, 'Curso Superior', '/admin/curso_superior', 2, 2, 4, 1, ''),
(24, 'Paralelos Tutores', '/admin/paralelos_tutores', 2, 3, 4, 1, ''),
(25, 'Paralelos Inspectores', '/admin/paralelos_inspectores', 2, 4, 4, 1, ''),
(26, 'Periodos', '/admin/cierre_periodos', 2, 1, 5, 1, ''),
(27, 'Definiciones', '#', 1, 1, 0, 1, ''),
(28, 'Horarios', '#', 1, 2, 0, 1, ''),
(29, 'Reportes', '#', 1, 3, 0, 1, ''),
(30, 'Consultas', '#', 1, 4, 0, 1, ''),
(31, 'Estadísticas', '#', 1, 5, 0, 1, ''),
(32, 'Malla Curricular', '/autoridad/malla_curricular', 2, 1, 27, 1, ''),
(33, 'Distributivo', '/autoridad/distributivo', 2, 2, 27, 1, ''),
(34, 'Definir Días de la Semana', '/autoridad/dias_semana', 2, 1, 28, 1, ''),
(35, 'Definir Horas Clase', '/autoridad/horas_clase', 2, 2, 28, 1, ''),
(36, 'Asociar Dia-Hora', '/autoridad/dia_hora', 2, 3, 28, 1, ''),
(37, 'Definir Horario Semanal', '/autoridad/horario_semanal', 2, 4, 28, 1, ''),
(38, 'Definir Inasistencias', '/autoridad/inasistencias', 2, 5, 28, 1, ''),
(39, 'Parciales', '/autoridad/reporte_parciales', 2, 1, 29, 1, ''),
(40, 'Quimestrales', '/autoridad/reporte_quimestral', 2, 2, 29, 1, ''),
(41, 'Anuales', '/autoridad/reporte_anual', 2, 3, 29, 1, ''),
(42, 'Lista de docentes', '/autoridad/lista_docentes', 2, 1, 30, 1, ''),
(43, 'Horarios de clase', '/autoridad/horario_clases', 2, 2, 30, 1, ''),
(44, 'Aprobados por Paralelo', '/autoridad/aprobados_paralelo', 2, 1, 31, 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_menu_perfil`
--

CREATE TABLE `sw_menu_perfil` (
  `id_perfil` int(11) UNSIGNED NOT NULL,
  `id_menu` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_menu_perfil`
--

INSERT INTO `sw_menu_perfil` (`id_perfil`, `id_menu`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(2, 27),
(2, 28),
(2, 29),
(2, 30),
(2, 31),
(2, 32),
(2, 33),
(2, 34),
(2, 35),
(2, 36),
(2, 37),
(2, 38),
(2, 39),
(2, 40),
(2, 41),
(2, 42),
(2, 43),
(2, 44);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_modalidad`
--

CREATE TABLE `sw_modalidad` (
  `id_modalidad` int(11) UNSIGNED NOT NULL,
  `mo_nombre` varchar(64) NOT NULL,
  `mo_activo` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_modalidad`
--

INSERT INTO `sw_modalidad` (`id_modalidad`, `mo_nombre`, `mo_activo`) VALUES
(1, 'SEMIPRESENCIAL', 1),
(2, 'BGU INTENSIVO', 0),
(3, 'EGB SUPERIOR INTENSIVA', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_paralelo`
--

CREATE TABLE `sw_paralelo` (
  `id_paralelo` int(11) UNSIGNED NOT NULL,
  `id_periodo_lectivo` int(11) UNSIGNED NOT NULL,
  `id_curso` int(11) UNSIGNED NOT NULL,
  `id_jornada` int(11) UNSIGNED NOT NULL,
  `pa_nombre` varchar(5) NOT NULL,
  `pa_orden` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_paralelo`
--

INSERT INTO `sw_paralelo` (`id_paralelo`, `id_periodo_lectivo`, `id_curso`, `id_jornada`, `pa_nombre`, `pa_orden`) VALUES
(1, 1, 1, 3, 'A', 1),
(2, 1, 2, 3, 'A', 2),
(3, 1, 3, 3, 'A', 3),
(4, 1, 3, 3, 'B', 4),
(5, 1, 4, 3, 'A', 5),
(6, 1, 4, 3, 'B', 6),
(7, 1, 5, 3, 'A', 9),
(8, 1, 5, 3, 'B', 10),
(9, 1, 6, 3, 'A', 13),
(10, 1, 6, 3, 'B', 14),
(11, 1, 7, 3, 'A', 7),
(12, 1, 8, 3, 'A', 11),
(13, 1, 9, 3, 'A', 15),
(14, 1, 10, 3, 'A', 8),
(15, 1, 11, 3, 'A', 12),
(16, 1, 12, 3, 'A', 16);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_perfil`
--

CREATE TABLE `sw_perfil` (
  `id_perfil` int(11) UNSIGNED NOT NULL,
  `pe_nombre` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_perfil`
--

INSERT INTO `sw_perfil` (`id_perfil`, `pe_nombre`) VALUES
(1, 'Administrador'),
(2, 'Autoridad'),
(3, 'DECE'),
(4, 'Docente'),
(5, 'Inspección'),
(6, 'Representante'),
(7, 'Secretaría'),
(8, 'Tutor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_periodo_estado`
--

CREATE TABLE `sw_periodo_estado` (
  `id_periodo_estado` int(11) UNSIGNED NOT NULL,
  `pe_descripcion` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_periodo_estado`
--

INSERT INTO `sw_periodo_estado` (`id_periodo_estado`, `pe_descripcion`) VALUES
(1, 'ACTUAL'),
(2, 'TERMINADO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_periodo_evaluacion`
--

CREATE TABLE `sw_periodo_evaluacion` (
  `id_periodo_evaluacion` int(11) UNSIGNED NOT NULL,
  `id_periodo_lectivo` int(11) UNSIGNED NOT NULL,
  `id_tipo_periodo` int(11) UNSIGNED NOT NULL,
  `pe_nombre` varchar(24) NOT NULL,
  `pe_abreviatura` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_periodo_evaluacion`
--

INSERT INTO `sw_periodo_evaluacion` (`id_periodo_evaluacion`, `id_periodo_lectivo`, `id_tipo_periodo`, `pe_nombre`, `pe_abreviatura`) VALUES
(6, 1, 1, 'PRIMER QUIMESTRE', '1ER.Q.'),
(7, 1, 1, 'SEGUNDO QUIMESTRE', '2DO.Q.'),
(8, 1, 2, 'EXAMEN SUPLETORIO', 'SUP.'),
(9, 1, 3, 'EXAMEN REMEDIAL', 'REM.'),
(10, 1, 4, 'EXAMEN DE GRACIA', 'GRA.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_periodo_lectivo`
--

CREATE TABLE `sw_periodo_lectivo` (
  `id_periodo_lectivo` int(11) UNSIGNED NOT NULL,
  `id_periodo_estado` int(11) UNSIGNED NOT NULL,
  `id_modalidad` int(11) UNSIGNED NOT NULL,
  `pe_anio_inicio` int(5) UNSIGNED NOT NULL,
  `pe_anio_fin` int(5) UNSIGNED NOT NULL,
  `pe_fecha_inicio` date NOT NULL,
  `pe_fecha_fin` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_periodo_lectivo`
--

INSERT INTO `sw_periodo_lectivo` (`id_periodo_lectivo`, `id_periodo_estado`, `id_modalidad`, `pe_anio_inicio`, `pe_anio_fin`, `pe_fecha_inicio`, `pe_fecha_fin`) VALUES
(1, 1, 1, 2021, 2022, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_rubrica_evaluacion`
--

CREATE TABLE `sw_rubrica_evaluacion` (
  `id_rubrica_evaluacion` int(11) UNSIGNED NOT NULL,
  `id_aporte_evaluacion` int(11) UNSIGNED NOT NULL,
  `id_tipo_asignatura` int(1) UNSIGNED NOT NULL DEFAULT 1,
  `ru_nombre` varchar(24) NOT NULL,
  `ru_abreviatura` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_rubrica_evaluacion`
--

INSERT INTO `sw_rubrica_evaluacion` (`id_rubrica_evaluacion`, `id_aporte_evaluacion`, `id_tipo_asignatura`, `ru_nombre`, `ru_abreviatura`) VALUES
(1, 1, 1, 'TRABAJOS', 'TR'),
(2, 1, 1, 'SUMATIVA', 'SU'),
(3, 2, 1, 'TRABAJOS', 'TR'),
(4, 2, 1, 'SUMATIVA', 'SU'),
(5, 3, 1, 'PROYECTO QUIMESTRAL', 'PROY'),
(6, 4, 1, 'TRABAJOS', 'TR'),
(7, 4, 1, 'SUMATIVA', 'SU'),
(8, 5, 1, 'TRABAJOS', 'TR'),
(9, 5, 1, 'SUMATIVA', 'SU'),
(10, 6, 1, 'PROYECTO QUIMESTRAL', 'PROY'),
(11, 7, 1, 'EXAMEN SUPLETORIO', 'SUP'),
(12, 8, 1, 'EXAMEN REMEDIAL', 'REM'),
(13, 9, 1, 'EXAMEN DE GRACIA', 'GRA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_tipo_aporte`
--

CREATE TABLE `sw_tipo_aporte` (
  `id_tipo_aporte` int(11) UNSIGNED NOT NULL,
  `ta_descripcion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_tipo_aporte`
--

INSERT INTO `sw_tipo_aporte` (`id_tipo_aporte`, `ta_descripcion`) VALUES
(1, 'PARCIAL'),
(2, 'EXAMEN QUIMESTRAL'),
(3, 'SUPLETORIO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_tipo_asignatura`
--

CREATE TABLE `sw_tipo_asignatura` (
  `id_tipo_asignatura` int(11) UNSIGNED NOT NULL,
  `ta_descripcion` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_tipo_asignatura`
--

INSERT INTO `sw_tipo_asignatura` (`id_tipo_asignatura`, `ta_descripcion`) VALUES
(1, 'CUANTITATIVA'),
(2, 'CUALITATIVA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_tipo_educacion`
--

CREATE TABLE `sw_tipo_educacion` (
  `id_tipo_educacion` int(11) UNSIGNED NOT NULL,
  `te_nombre` varchar(48) NOT NULL,
  `te_bachillerato` int(1) UNSIGNED NOT NULL,
  `te_orden` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_tipo_educacion`
--

INSERT INTO `sw_tipo_educacion` (`id_tipo_educacion`, `te_nombre`, `te_bachillerato`, `te_orden`) VALUES
(1, 'Educación General Básica Preparatoria', 0, 1),
(2, 'Educación General Básica Elemental', 0, 2),
(3, 'Educación General Básica Media', 0, 3),
(4, 'Educación General Básica Superior', 0, 4),
(5, 'Bachillerato General Unificado', 1, 5),
(6, 'BACHILLERATO TECNICO', 1, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_tipo_periodo`
--

CREATE TABLE `sw_tipo_periodo` (
  `id_tipo_periodo` int(11) UNSIGNED NOT NULL,
  `tp_descripcion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_tipo_periodo`
--

INSERT INTO `sw_tipo_periodo` (`id_tipo_periodo`, `tp_descripcion`) VALUES
(1, 'QUIMESTRE'),
(2, 'SUPLETORIO'),
(3, 'REMEDIAL'),
(4, 'DE GRACIA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_usuario`
--

CREATE TABLE `sw_usuario` (
  `id_usuario` int(11) UNSIGNED NOT NULL,
  `us_titulo` varchar(8) NOT NULL,
  `us_apellidos` varchar(32) NOT NULL,
  `us_nombres` varchar(32) NOT NULL,
  `us_shortname` varchar(45) NOT NULL,
  `us_fullname` varchar(64) NOT NULL,
  `us_login` varchar(24) NOT NULL,
  `us_password` varchar(535) NOT NULL,
  `us_foto` varchar(100) NOT NULL,
  `us_genero` varchar(1) NOT NULL,
  `us_activo` int(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_usuario`
--

INSERT INTO `sw_usuario` (`id_usuario`, `us_titulo`, `us_apellidos`, `us_nombres`, `us_shortname`, `us_fullname`, `us_login`, `us_password`, `us_foto`, `us_genero`, `us_activo`) VALUES
(1, 'Ing.', 'Peñaherrera Escobar', 'Gonzalo Nicolás', 'Ing. Gonzalo Peñaherrera', 'Peñaherrera Escobar Gonzalo Nicolás', 'Administrador', '$2y$10$yrnNcWfjbFwjyHfdMvqD/OAa/IMIhbXS1I2EMes7q3u7xR.usBPui', 'gonzalo-foto-3.png', 'M', 1),
(2, 'Mg.', 'ACOSTA VENENAULA ', 'MERCY NATHALY', 'Mg. MERCY ACOSTA', 'ACOSTA VENENAULA  MERCY NATHALY', 'macosta', '$2y$10$njg91EmWBctIm8nkSiwI3ubMmXqu63OB4z.GBuw02vH0MbOXlrTA2', '1626296482_650ab0319b5b1350a8c5.jpg', 'F', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_usuario_perfil`
--

CREATE TABLE `sw_usuario_perfil` (
  `id_usuario` int(11) UNSIGNED NOT NULL,
  `id_perfil` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_usuario_perfil`
--

INSERT INTO `sw_usuario_perfil` (`id_usuario`, `id_perfil`) VALUES
(1, 1),
(2, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sw_aporte_evaluacion`
--
ALTER TABLE `sw_aporte_evaluacion`
  ADD PRIMARY KEY (`id_aporte_evaluacion`),
  ADD KEY `sw_aporte_evaluacion_id_periodo_evaluacion_foreign` (`id_periodo_evaluacion`),
  ADD KEY `sw_aporte_evaluacion_id_tipo_aporte_foreign` (`id_tipo_aporte`);

--
-- Indices de la tabla `sw_area`
--
ALTER TABLE `sw_area`
  ADD PRIMARY KEY (`id_area`);

--
-- Indices de la tabla `sw_asignatura`
--
ALTER TABLE `sw_asignatura`
  ADD PRIMARY KEY (`id_asignatura`),
  ADD KEY `sw_asignatura_id_area_foreign` (`id_area`),
  ADD KEY `sw_asignatura_id_tipo_asignatura_foreign` (`id_tipo_asignatura`);

--
-- Indices de la tabla `sw_curso`
--
ALTER TABLE `sw_curso`
  ADD PRIMARY KEY (`id_curso`),
  ADD KEY `sw_curso_id_especialidad_foreign` (`id_especialidad`);

--
-- Indices de la tabla `sw_especialidad`
--
ALTER TABLE `sw_especialidad`
  ADD PRIMARY KEY (`id_especialidad`),
  ADD KEY `sw_especialidad_id_tipo_educacion_foreign` (`id_tipo_educacion`);

--
-- Indices de la tabla `sw_institucion`
--
ALTER TABLE `sw_institucion`
  ADD PRIMARY KEY (`id_institucion`);

--
-- Indices de la tabla `sw_jornada`
--
ALTER TABLE `sw_jornada`
  ADD PRIMARY KEY (`id_jornada`);

--
-- Indices de la tabla `sw_menu`
--
ALTER TABLE `sw_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indices de la tabla `sw_menu_perfil`
--
ALTER TABLE `sw_menu_perfil`
  ADD KEY `sw_menu_perfil_id_perfil_foreign` (`id_perfil`),
  ADD KEY `sw_menu_perfil_id_menu_foreign` (`id_menu`);

--
-- Indices de la tabla `sw_modalidad`
--
ALTER TABLE `sw_modalidad`
  ADD PRIMARY KEY (`id_modalidad`);

--
-- Indices de la tabla `sw_paralelo`
--
ALTER TABLE `sw_paralelo`
  ADD PRIMARY KEY (`id_paralelo`),
  ADD KEY `sw_paralelo_id_periodo_lectivo_foreign` (`id_periodo_lectivo`),
  ADD KEY `sw_paralelo_id_curso_foreign` (`id_curso`),
  ADD KEY `sw_paralelo_id_jornada_foreign` (`id_jornada`);

--
-- Indices de la tabla `sw_perfil`
--
ALTER TABLE `sw_perfil`
  ADD PRIMARY KEY (`id_perfil`);

--
-- Indices de la tabla `sw_periodo_estado`
--
ALTER TABLE `sw_periodo_estado`
  ADD PRIMARY KEY (`id_periodo_estado`);

--
-- Indices de la tabla `sw_periodo_evaluacion`
--
ALTER TABLE `sw_periodo_evaluacion`
  ADD PRIMARY KEY (`id_periodo_evaluacion`),
  ADD KEY `sw_periodo_evaluacion_id_periodo_lectivo_foreign` (`id_periodo_lectivo`),
  ADD KEY `sw_periodo_evaluacion_id_tipo_periodo_foreign` (`id_tipo_periodo`);

--
-- Indices de la tabla `sw_periodo_lectivo`
--
ALTER TABLE `sw_periodo_lectivo`
  ADD PRIMARY KEY (`id_periodo_lectivo`),
  ADD KEY `sw_periodo_lectivo_id_periodo_estado_foreign` (`id_periodo_estado`),
  ADD KEY `sw_periodo_lectivo_id_modalidad_foreign` (`id_modalidad`);

--
-- Indices de la tabla `sw_rubrica_evaluacion`
--
ALTER TABLE `sw_rubrica_evaluacion`
  ADD PRIMARY KEY (`id_rubrica_evaluacion`),
  ADD KEY `sw_rubrica_evaluacion_id_aporte_evaluacion_foreign` (`id_aporte_evaluacion`),
  ADD KEY `sw_rubrica_evaluacion_id_tipo_asignatura_foreign` (`id_tipo_asignatura`);

--
-- Indices de la tabla `sw_tipo_aporte`
--
ALTER TABLE `sw_tipo_aporte`
  ADD PRIMARY KEY (`id_tipo_aporte`);

--
-- Indices de la tabla `sw_tipo_asignatura`
--
ALTER TABLE `sw_tipo_asignatura`
  ADD PRIMARY KEY (`id_tipo_asignatura`);

--
-- Indices de la tabla `sw_tipo_educacion`
--
ALTER TABLE `sw_tipo_educacion`
  ADD PRIMARY KEY (`id_tipo_educacion`);

--
-- Indices de la tabla `sw_tipo_periodo`
--
ALTER TABLE `sw_tipo_periodo`
  ADD PRIMARY KEY (`id_tipo_periodo`);

--
-- Indices de la tabla `sw_usuario`
--
ALTER TABLE `sw_usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `sw_usuario_perfil`
--
ALTER TABLE `sw_usuario_perfil`
  ADD KEY `sw_usuario_perfil_id_usuario_foreign` (`id_usuario`),
  ADD KEY `sw_usuario_perfil_id_perfil_foreign` (`id_perfil`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT de la tabla `sw_aporte_evaluacion`
--
ALTER TABLE `sw_aporte_evaluacion`
  MODIFY `id_aporte_evaluacion` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `sw_area`
--
ALTER TABLE `sw_area`
  MODIFY `id_area` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `sw_asignatura`
--
ALTER TABLE `sw_asignatura`
  MODIFY `id_asignatura` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `sw_curso`
--
ALTER TABLE `sw_curso`
  MODIFY `id_curso` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `sw_especialidad`
--
ALTER TABLE `sw_especialidad`
  MODIFY `id_especialidad` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `sw_institucion`
--
ALTER TABLE `sw_institucion`
  MODIFY `id_institucion` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `sw_jornada`
--
ALTER TABLE `sw_jornada`
  MODIFY `id_jornada` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `sw_menu`
--
ALTER TABLE `sw_menu`
  MODIFY `id_menu` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT de la tabla `sw_modalidad`
--
ALTER TABLE `sw_modalidad`
  MODIFY `id_modalidad` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `sw_paralelo`
--
ALTER TABLE `sw_paralelo`
  MODIFY `id_paralelo` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `sw_perfil`
--
ALTER TABLE `sw_perfil`
  MODIFY `id_perfil` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `sw_periodo_estado`
--
ALTER TABLE `sw_periodo_estado`
  MODIFY `id_periodo_estado` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `sw_periodo_evaluacion`
--
ALTER TABLE `sw_periodo_evaluacion`
  MODIFY `id_periodo_evaluacion` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `sw_periodo_lectivo`
--
ALTER TABLE `sw_periodo_lectivo`
  MODIFY `id_periodo_lectivo` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `sw_rubrica_evaluacion`
--
ALTER TABLE `sw_rubrica_evaluacion`
  MODIFY `id_rubrica_evaluacion` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `sw_tipo_aporte`
--
ALTER TABLE `sw_tipo_aporte`
  MODIFY `id_tipo_aporte` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `sw_tipo_asignatura`
--
ALTER TABLE `sw_tipo_asignatura`
  MODIFY `id_tipo_asignatura` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `sw_tipo_educacion`
--
ALTER TABLE `sw_tipo_educacion`
  MODIFY `id_tipo_educacion` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `sw_tipo_periodo`
--
ALTER TABLE `sw_tipo_periodo`
  MODIFY `id_tipo_periodo` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `sw_usuario`
--
ALTER TABLE `sw_usuario`
  MODIFY `id_usuario` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `sw_aporte_evaluacion`
--
ALTER TABLE `sw_aporte_evaluacion`
  ADD CONSTRAINT `sw_aporte_evaluacion_id_periodo_evaluacion_foreign` FOREIGN KEY (`id_periodo_evaluacion`) REFERENCES `sw_periodo_evaluacion` (`id_periodo_evaluacion`),
  ADD CONSTRAINT `sw_aporte_evaluacion_id_tipo_aporte_foreign` FOREIGN KEY (`id_tipo_aporte`) REFERENCES `sw_tipo_aporte` (`id_tipo_aporte`);

--
-- Filtros para la tabla `sw_asignatura`
--
ALTER TABLE `sw_asignatura`
  ADD CONSTRAINT `sw_asignatura_id_area_foreign` FOREIGN KEY (`id_area`) REFERENCES `sw_area` (`id_area`),
  ADD CONSTRAINT `sw_asignatura_id_tipo_asignatura_foreign` FOREIGN KEY (`id_tipo_asignatura`) REFERENCES `sw_tipo_asignatura` (`id_tipo_asignatura`);

--
-- Filtros para la tabla `sw_curso`
--
ALTER TABLE `sw_curso`
  ADD CONSTRAINT `sw_curso_id_especialidad_foreign` FOREIGN KEY (`id_especialidad`) REFERENCES `sw_especialidad` (`id_especialidad`);

--
-- Filtros para la tabla `sw_especialidad`
--
ALTER TABLE `sw_especialidad`
  ADD CONSTRAINT `sw_especialidad_id_tipo_educacion_foreign` FOREIGN KEY (`id_tipo_educacion`) REFERENCES `sw_tipo_educacion` (`id_tipo_educacion`);

--
-- Filtros para la tabla `sw_menu_perfil`
--
ALTER TABLE `sw_menu_perfil`
  ADD CONSTRAINT `sw_menu_perfil_id_menu_foreign` FOREIGN KEY (`id_menu`) REFERENCES `sw_menu` (`id_menu`),
  ADD CONSTRAINT `sw_menu_perfil_id_perfil_foreign` FOREIGN KEY (`id_perfil`) REFERENCES `sw_perfil` (`id_perfil`);

--
-- Filtros para la tabla `sw_paralelo`
--
ALTER TABLE `sw_paralelo`
  ADD CONSTRAINT `sw_paralelo_id_curso_foreign` FOREIGN KEY (`id_curso`) REFERENCES `sw_curso` (`id_curso`),
  ADD CONSTRAINT `sw_paralelo_id_jornada_foreign` FOREIGN KEY (`id_jornada`) REFERENCES `sw_jornada` (`id_jornada`),
  ADD CONSTRAINT `sw_paralelo_id_periodo_lectivo_foreign` FOREIGN KEY (`id_periodo_lectivo`) REFERENCES `sw_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Filtros para la tabla `sw_periodo_evaluacion`
--
ALTER TABLE `sw_periodo_evaluacion`
  ADD CONSTRAINT `sw_periodo_evaluacion_id_periodo_lectivo_foreign` FOREIGN KEY (`id_periodo_lectivo`) REFERENCES `sw_periodo_lectivo` (`id_periodo_lectivo`),
  ADD CONSTRAINT `sw_periodo_evaluacion_id_tipo_periodo_foreign` FOREIGN KEY (`id_tipo_periodo`) REFERENCES `sw_tipo_periodo` (`id_tipo_periodo`);

--
-- Filtros para la tabla `sw_periodo_lectivo`
--
ALTER TABLE `sw_periodo_lectivo`
  ADD CONSTRAINT `sw_periodo_lectivo_id_modalidad_foreign` FOREIGN KEY (`id_modalidad`) REFERENCES `sw_modalidad` (`id_modalidad`),
  ADD CONSTRAINT `sw_periodo_lectivo_id_periodo_estado_foreign` FOREIGN KEY (`id_periodo_estado`) REFERENCES `sw_periodo_estado` (`id_periodo_estado`);

--
-- Filtros para la tabla `sw_rubrica_evaluacion`
--
ALTER TABLE `sw_rubrica_evaluacion`
  ADD CONSTRAINT `sw_rubrica_evaluacion_id_aporte_evaluacion_foreign` FOREIGN KEY (`id_aporte_evaluacion`) REFERENCES `sw_aporte_evaluacion` (`id_aporte_evaluacion`),
  ADD CONSTRAINT `sw_rubrica_evaluacion_id_tipo_asignatura_foreign` FOREIGN KEY (`id_tipo_asignatura`) REFERENCES `sw_tipo_asignatura` (`id_tipo_asignatura`);

--
-- Filtros para la tabla `sw_usuario_perfil`
--
ALTER TABLE `sw_usuario_perfil`
  ADD CONSTRAINT `sw_usuario_perfil_id_perfil_foreign` FOREIGN KEY (`id_perfil`) REFERENCES `sw_perfil` (`id_perfil`),
  ADD CONSTRAINT `sw_usuario_perfil_id_usuario_foreign` FOREIGN KEY (`id_usuario`) REFERENCES `sw_usuario` (`id_usuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
